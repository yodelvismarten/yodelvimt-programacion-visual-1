﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep
{
    public partial class formCheckBoxList : Form
    {
        public formCheckBoxList()
        {
            InitializeComponent();
            llenarCheckBoxList();
        }

        public void llenarCheckBoxList() {
            
            //LISTA TIPO STRING DE LENGUAJES DE PROGRAMACION
            List<string> listaLenguajes = new List<string>();
            
            //LLENADO LISTA
            listaLenguajes.Add("Python");
            listaLenguajes.Add("C#");
            listaLenguajes.Add("C++");
            listaLenguajes.Add("Java");
            listaLenguajes.Add("Perl");
            listaLenguajes.Add("Ruby");
            listaLenguajes.Add("JavaScript");
            listaLenguajes.Add("Go");
            listaLenguajes.Add("Kotlin");
            listaLenguajes.Add("PHP");

            //AGREGAR ELEMENTOS DE LA LISTA A CHECKBOXLIST
            foreach (var item in listaLenguajes) {
                this.checkedListBox1.Items.Add(item);
            }
                    
        }

        public void checkBoxChecked() {
            //VARIABLE PARA ALMACENAR EL MENSAJE A MOSTRAR
            string mensaje = "";

            //SELECCIONAR CADA CHECK BOX SELECCIONADO EN EL CHECKBOXLIST Y CONCATENARLOS A LA VARIABLE MENSAJE
            foreach (var item in checkedListBox1.CheckedItems)
            {
                mensaje += "-" + item.ToString() + "\n";
            }
            //MENSAJE A MOSTRAR
            MessageBox.Show("Sus lenguajes preferidos son: \n" + mensaje, "informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnMensaje_Click(object sender, EventArgs e)
        {
            checkBoxChecked();
        }
    }
}
