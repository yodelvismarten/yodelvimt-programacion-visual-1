﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Yodelvi_ProyectoInfotep
{
    public partial class formValidarNumeroTelefono : Form
    {
        public formValidarNumeroTelefono()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNumeroTelefonico_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) 
            {
                string numeroTel = txtNumeroTelefonico.Text;

                string expresionRegular = @"\A((1\s)?)\((809|829|849)\)(\s)([0-9]{3})(\-)([0-9]{4})\Z";
                Regex validacionRE = new Regex(expresionRegular);

                if (validacionRE.IsMatch(numeroTel))
                {

                    MessageBox.Show("Numero Correcto","Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else 
                {
                    MessageBox.Show("Numero Incorrecto","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
