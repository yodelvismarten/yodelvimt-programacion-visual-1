﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep
{
    public partial class FormLinkLabel : Form
    {
        public FormLinkLabel()
        {
            InitializeComponent();
        }

        private void lblGoogle_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.google.com/");
        }

        private void btnFavoritos_Click(object sender, EventArgs e)
        {
            new FormListaPaginasFavoritas().Show();
        }

        private void btnNavegador_Click(object sender, EventArgs e)
        {
            new FormNavegador().Show();
        }
    }
}
