﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Yodelvi_ProyectoInfotep
{
    public partial class formValidarCorreo : Form
    {
        public formValidarCorreo()
        {
            InitializeComponent();
        }

        private void txtCorreo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) 
            {
                string contenidoTxt = txtCorreo.Text;
                string expresionRegular = @"\A(\w+)(\@)(\w+)(\.)(com|org|edu|info)\Z";
                Regex validacionRE = new Regex(expresionRegular);

                if (validacionRE.IsMatch(contenidoTxt))
                {
                    MessageBox.Show("Correo correcto", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                else 
                {
                    MessageBox.Show("Correo incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void formValidarCorreo_Load(object sender, EventArgs e)
        {

        }
    }
}
