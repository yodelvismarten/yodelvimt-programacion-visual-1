﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        public void iniciarSesion() {
            if (txtUsuario.Text == "User" && txtPassword.Text == "1234")
            {
                new Form1().Show();
                this.Hide();
            }

            else {
                txtError.Text = "Usuario o Clave Incorrectos";
                txtError.Visible = true;
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            iniciarSesion();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
