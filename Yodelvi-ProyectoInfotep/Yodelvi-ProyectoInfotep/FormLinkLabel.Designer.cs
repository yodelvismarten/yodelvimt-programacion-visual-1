﻿namespace Yodelvi_ProyectoInfotep
{
    partial class FormLinkLabel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblGoogle = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFavoritos = new System.Windows.Forms.Button();
            this.btnNavegador = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Visitar Google";
            // 
            // lblGoogle
            // 
            this.lblGoogle.AutoSize = true;
            this.lblGoogle.Location = new System.Drawing.Point(6, 55);
            this.lblGoogle.Name = "lblGoogle";
            this.lblGoogle.Size = new System.Drawing.Size(64, 13);
            this.lblGoogle.TabIndex = 1;
            this.lblGoogle.TabStop = true;
            this.lblGoogle.Text = "Google.com";
            this.lblGoogle.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblGoogle_LinkClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblGoogle);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(85, 85);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ir Google";
            // 
            // btnFavoritos
            // 
            this.btnFavoritos.Location = new System.Drawing.Point(103, 17);
            this.btnFavoritos.Name = "btnFavoritos";
            this.btnFavoritos.Size = new System.Drawing.Size(94, 23);
            this.btnFavoritos.TabIndex = 3;
            this.btnFavoritos.Text = "Sitios Favoritos";
            this.btnFavoritos.UseVisualStyleBackColor = true;
            this.btnFavoritos.Click += new System.EventHandler(this.btnFavoritos_Click);
            // 
            // btnNavegador
            // 
            this.btnNavegador.Location = new System.Drawing.Point(103, 74);
            this.btnNavegador.Name = "btnNavegador";
            this.btnNavegador.Size = new System.Drawing.Size(94, 23);
            this.btnNavegador.TabIndex = 4;
            this.btnNavegador.Text = "Navegador";
            this.btnNavegador.UseVisualStyleBackColor = true;
            this.btnNavegador.Click += new System.EventHandler(this.btnNavegador_Click);
            // 
            // FormLinkLabel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(203, 158);
            this.Controls.Add(this.btnNavegador);
            this.Controls.Add(this.btnFavoritos);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormLinkLabel";
            this.Text = "FormLinkLabel";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel lblGoogle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnFavoritos;
        private System.Windows.Forms.Button btnNavegador;
    }
}