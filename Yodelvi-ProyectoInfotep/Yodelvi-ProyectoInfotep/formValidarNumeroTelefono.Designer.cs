﻿namespace Yodelvi_ProyectoInfotep
{
    partial class formValidarNumeroTelefono
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNumeroTelefonico = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtNumeroTelefonico
            // 
            this.txtNumeroTelefonico.Location = new System.Drawing.Point(69, 74);
            this.txtNumeroTelefonico.Name = "txtNumeroTelefonico";
            this.txtNumeroTelefonico.Size = new System.Drawing.Size(182, 20);
            this.txtNumeroTelefonico.TabIndex = 0;
            this.txtNumeroTelefonico.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtNumeroTelefonico.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtNumeroTelefonico_KeyUp);
            // 
            // formValidarNumeroTelefono
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 174);
            this.Controls.Add(this.txtNumeroTelefonico);
            this.Name = "formValidarNumeroTelefono";
            this.Text = "formValidarNumeroTelefono";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNumeroTelefonico;
    }
}