﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep
{
    public partial class FormNavegador : Form
    {
        public FormNavegador()
        {
            InitializeComponent();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter) { 
                buscar();
            }
        }

        public void buscar() {
            string busqueda = textBox1.Text;
            System.Diagnostics.Process.Start("https://www.google.com/search?hl=" + busqueda);

        }
    }
}
