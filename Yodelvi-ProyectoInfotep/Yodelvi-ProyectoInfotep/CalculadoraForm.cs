﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Yodelvi_ProyectoInfotep
{
    public partial class CalculadoraForm : Form
    {
        public CalculadoraForm()
        {
            InitializeComponent();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string contenidoTextBox = txtMostrar.Text;

                //Esta expresion regular tiene que ser cumplida completamente (\A , \Z) y acepta el siguiente patron: 
                //   1 - cifra entera-> (\d*)
                //   2 - puede tener o no un espacio en blanco-> (\s?)
                //   3 - cualquier operador aritmetico -> (\+|\-|\*|\/)
                //   4 - puede tener o no un espacio en blanco-> (\s?)
                //   5 - cifra entera-> (\d*)

                string expresionRegular = @"\A(\d*)(\s?)(\+|\-|\*|\/)(\s?)(\d*)\Z";
                Regex validacionCadena = new Regex(expresionRegular);

                if (validacionCadena.IsMatch(contenidoTextBox))
                {
                    if (contenidoTextBox.Contains('+'))
                    {
                        var valores = contenidoTextBox.Split('+');

                        int valor1 = int.Parse(valores[0]);
                        int valor2 = int.Parse(valores[1]);
                        string resultado = (valor1 + valor2).ToString();
                        txtMostrar.Text = resultado;
                    }

                    else if (contenidoTextBox.Contains('-'))
                    {
                        var valores = contenidoTextBox.Split('-');

                        int valor1 = int.Parse(valores[0]);
                        int valor2 = int.Parse(valores[1]);
                        string resultado = (valor1 - valor2).ToString();
                        txtMostrar.Text = resultado;
                    }

                    else if (contenidoTextBox.Contains('*'))
                    {
                        var valores = contenidoTextBox.Split('*');
                        int valor1 = int.Parse(valores[0]);
                        int valor2 = int.Parse(valores[1]);
                        string resultado = (valor1 * valor2).ToString();
                        txtMostrar.Text = resultado;

                    }

                    else if (contenidoTextBox.Contains('/'))
                    {
                        var valores = contenidoTextBox.Split('/');

                        int valor1 = int.Parse(valores[0]);
                        int valor2 = int.Parse(valores[1]);
                        string resultado = (valor1 / valor2).ToString();
                        txtMostrar.Text = resultado;
                    }

                }
                else 
                {
                    MessageBox.Show("Valores ingresados erroneamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
