﻿namespace Yodelvi_ProyectoInfotep
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtColor = new System.Windows.Forms.TextBox();
            this.txtCantidadPatas = new System.Windows.Forms.TextBox();
            this.txtGenero = new System.Windows.Forms.TextBox();
            this.txtRaza = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCorrer = new System.Windows.Forms.Button();
            this.btnDormir = new System.Windows.Forms.Button();
            this.btnSentarse = new System.Windows.Forms.Button();
            this.btnLadrar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnValidarID = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bntCorreo = new System.Windows.Forms.GroupBox();
            this.btnLinkLabel = new System.Windows.Forms.Button();
            this.btnFecha = new System.Windows.Forms.Button();
            this.btnJuegoLabels = new System.Windows.Forms.Button();
            this.btnCBoxCarros = new System.Windows.Forms.Button();
            this.checkBList = new System.Windows.Forms.Button();
            this.btnCrud = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btnFlowLayout = new System.Windows.Forms.Button();
            this.btnNumeroTelefonico = new System.Windows.Forms.Button();
            this.btnValidarUrl = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnConexionBD = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.bntCorreo.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtColor
            // 
            this.txtColor.Location = new System.Drawing.Point(43, 26);
            this.txtColor.Name = "txtColor";
            this.txtColor.Size = new System.Drawing.Size(100, 20);
            this.txtColor.TabIndex = 4;
            // 
            // txtCantidadPatas
            // 
            this.txtCantidadPatas.Location = new System.Drawing.Point(43, 58);
            this.txtCantidadPatas.Name = "txtCantidadPatas";
            this.txtCantidadPatas.Size = new System.Drawing.Size(100, 20);
            this.txtCantidadPatas.TabIndex = 5;
            // 
            // txtGenero
            // 
            this.txtGenero.Location = new System.Drawing.Point(197, 23);
            this.txtGenero.Name = "txtGenero";
            this.txtGenero.Size = new System.Drawing.Size(100, 20);
            this.txtGenero.TabIndex = 6;
            // 
            // txtRaza
            // 
            this.txtRaza.Location = new System.Drawing.Point(197, 58);
            this.txtRaza.Name = "txtRaza";
            this.txtRaza.Size = new System.Drawing.Size(100, 20);
            this.txtRaza.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Color";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Patas";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(149, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Genero";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(159, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Raza";
            // 
            // btnCorrer
            // 
            this.btnCorrer.Location = new System.Drawing.Point(9, 120);
            this.btnCorrer.Name = "btnCorrer";
            this.btnCorrer.Size = new System.Drawing.Size(75, 23);
            this.btnCorrer.TabIndex = 12;
            this.btnCorrer.Text = "Correr";
            this.btnCorrer.UseVisualStyleBackColor = true;
            this.btnCorrer.Click += new System.EventHandler(this.btnCorrer_Click);
            // 
            // btnDormir
            // 
            this.btnDormir.Location = new System.Drawing.Point(171, 120);
            this.btnDormir.Name = "btnDormir";
            this.btnDormir.Size = new System.Drawing.Size(75, 23);
            this.btnDormir.TabIndex = 14;
            this.btnDormir.Text = "Dormir";
            this.btnDormir.UseVisualStyleBackColor = true;
            this.btnDormir.Click += new System.EventHandler(this.btnDormir_Click);
            // 
            // btnSentarse
            // 
            this.btnSentarse.Location = new System.Drawing.Point(252, 120);
            this.btnSentarse.Name = "btnSentarse";
            this.btnSentarse.Size = new System.Drawing.Size(75, 23);
            this.btnSentarse.TabIndex = 15;
            this.btnSentarse.Text = "Sentarse";
            this.btnSentarse.UseVisualStyleBackColor = true;
            this.btnSentarse.Click += new System.EventHandler(this.btnSentarse_Click);
            // 
            // btnLadrar
            // 
            this.btnLadrar.Location = new System.Drawing.Point(90, 120);
            this.btnLadrar.Name = "btnLadrar";
            this.btnLadrar.Size = new System.Drawing.Size(75, 23);
            this.btnLadrar.TabIndex = 16;
            this.btnLadrar.Text = "Ladrar";
            this.btnLadrar.UseVisualStyleBackColor = true;
            this.btnLadrar.Click += new System.EventHandler(this.btnLadrar_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(114, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Eventos Con Perro";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 45);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(74, 23);
            this.button1.TabIndex = 18;
            this.button1.Text = "Key Press";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(106, 45);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(74, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "Key Up";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(199, 45);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 20;
            this.button3.Text = "Calculadora";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnValidarID
            // 
            this.btnValidarID.Location = new System.Drawing.Point(293, 45);
            this.btnValidarID.Name = "btnValidarID";
            this.btnValidarID.Size = new System.Drawing.Size(75, 23);
            this.btnValidarID.TabIndex = 21;
            this.btnValidarID.Text = "Validar ID";
            this.btnValidarID.UseVisualStyleBackColor = true;
            this.btnValidarID.Click += new System.EventHandler(this.btnValidarID_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtColor);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtCantidadPatas);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnSentarse);
            this.groupBox1.Controls.Add(this.btnLadrar);
            this.groupBox1.Controls.Add(this.btnDormir);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtGenero);
            this.groupBox1.Controls.Add(this.txtRaza);
            this.groupBox1.Controls.Add(this.btnCorrer);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(121, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 161);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Clase Perro";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // bntCorreo
            // 
            this.bntCorreo.Controls.Add(this.btnConexionBD);
            this.bntCorreo.Controls.Add(this.btnLinkLabel);
            this.bntCorreo.Controls.Add(this.btnFecha);
            this.bntCorreo.Controls.Add(this.btnJuegoLabels);
            this.bntCorreo.Controls.Add(this.btnCBoxCarros);
            this.bntCorreo.Controls.Add(this.checkBList);
            this.bntCorreo.Controls.Add(this.btnCrud);
            this.bntCorreo.Controls.Add(this.button5);
            this.bntCorreo.Controls.Add(this.btnFlowLayout);
            this.bntCorreo.Controls.Add(this.btnNumeroTelefonico);
            this.bntCorreo.Controls.Add(this.btnValidarUrl);
            this.bntCorreo.Controls.Add(this.button4);
            this.bntCorreo.Controls.Add(this.btnValidarID);
            this.bntCorreo.Controls.Add(this.button1);
            this.bntCorreo.Controls.Add(this.button2);
            this.bntCorreo.Controls.Add(this.button3);
            this.bntCorreo.Location = new System.Drawing.Point(98, 179);
            this.bntCorreo.Name = "bntCorreo";
            this.bntCorreo.Size = new System.Drawing.Size(383, 202);
            this.bntCorreo.TabIndex = 23;
            this.bntCorreo.TabStop = false;
            this.bntCorreo.Text = "Otras Ventanas";
            // 
            // btnLinkLabel
            // 
            this.btnLinkLabel.Location = new System.Drawing.Point(199, 133);
            this.btnLinkLabel.Name = "btnLinkLabel";
            this.btnLinkLabel.Size = new System.Drawing.Size(75, 23);
            this.btnLinkLabel.TabIndex = 32;
            this.btnLinkLabel.Text = "Google";
            this.btnLinkLabel.UseVisualStyleBackColor = true;
            this.btnLinkLabel.Click += new System.EventHandler(this.btnLinkLabel_Click);
            // 
            // btnFecha
            // 
            this.btnFecha.Location = new System.Drawing.Point(12, 133);
            this.btnFecha.Name = "btnFecha";
            this.btnFecha.Size = new System.Drawing.Size(75, 23);
            this.btnFecha.TabIndex = 31;
            this.btnFecha.Text = "Fechas";
            this.btnFecha.UseVisualStyleBackColor = true;
            this.btnFecha.Click += new System.EventHandler(this.btnFecha_Click);
            // 
            // btnJuegoLabels
            // 
            this.btnJuegoLabels.Location = new System.Drawing.Point(106, 133);
            this.btnJuegoLabels.Name = "btnJuegoLabels";
            this.btnJuegoLabels.Size = new System.Drawing.Size(75, 23);
            this.btnJuegoLabels.TabIndex = 30;
            this.btnJuegoLabels.Text = "Juego Label";
            this.btnJuegoLabels.UseVisualStyleBackColor = true;
            this.btnJuegoLabels.Click += new System.EventHandler(this.btnJuegoLabels_Click);
            // 
            // btnCBoxCarros
            // 
            this.btnCBoxCarros.Location = new System.Drawing.Point(293, 103);
            this.btnCBoxCarros.Name = "btnCBoxCarros";
            this.btnCBoxCarros.Size = new System.Drawing.Size(75, 23);
            this.btnCBoxCarros.TabIndex = 29;
            this.btnCBoxCarros.Text = "CBox Carros";
            this.btnCBoxCarros.UseVisualStyleBackColor = true;
            this.btnCBoxCarros.Click += new System.EventHandler(this.btnCBoxCarros_Click);
            // 
            // checkBList
            // 
            this.checkBList.Location = new System.Drawing.Point(199, 105);
            this.checkBList.Name = "checkBList";
            this.checkBList.Size = new System.Drawing.Size(75, 23);
            this.checkBList.TabIndex = 28;
            this.checkBList.Text = "CheckB List";
            this.checkBList.UseVisualStyleBackColor = true;
            this.checkBList.Click += new System.EventHandler(this.checkBList_Click);
            // 
            // btnCrud
            // 
            this.btnCrud.Location = new System.Drawing.Point(106, 104);
            this.btnCrud.Name = "btnCrud";
            this.btnCrud.Size = new System.Drawing.Size(75, 23);
            this.btnCrud.TabIndex = 27;
            this.btnCrud.Text = "CRUD";
            this.btnCrud.UseVisualStyleBackColor = true;
            this.btnCrud.Click += new System.EventHandler(this.btnCrud_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(13, 105);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 26;
            this.button5.Text = "Table L.";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnFlowLayout
            // 
            this.btnFlowLayout.Location = new System.Drawing.Point(293, 74);
            this.btnFlowLayout.Name = "btnFlowLayout";
            this.btnFlowLayout.Size = new System.Drawing.Size(75, 23);
            this.btnFlowLayout.TabIndex = 25;
            this.btnFlowLayout.Text = "Flow Layout";
            this.btnFlowLayout.UseVisualStyleBackColor = true;
            this.btnFlowLayout.Click += new System.EventHandler(this.btnFlowLayout_Click);
            // 
            // btnNumeroTelefonico
            // 
            this.btnNumeroTelefonico.Location = new System.Drawing.Point(199, 76);
            this.btnNumeroTelefonico.Name = "btnNumeroTelefonico";
            this.btnNumeroTelefonico.Size = new System.Drawing.Size(75, 23);
            this.btnNumeroTelefonico.TabIndex = 24;
            this.btnNumeroTelefonico.Text = "Numero Tel.";
            this.btnNumeroTelefonico.UseVisualStyleBackColor = true;
            this.btnNumeroTelefonico.Click += new System.EventHandler(this.btnNumeroTelefonico_Click);
            // 
            // btnValidarUrl
            // 
            this.btnValidarUrl.Location = new System.Drawing.Point(106, 74);
            this.btnValidarUrl.Name = "btnValidarUrl";
            this.btnValidarUrl.Size = new System.Drawing.Size(74, 23);
            this.btnValidarUrl.TabIndex = 23;
            this.btnValidarUrl.Text = "URL";
            this.btnValidarUrl.UseVisualStyleBackColor = true;
            this.btnValidarUrl.Click += new System.EventHandler(this.btnValidarUrl_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(13, 76);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 22;
            this.button4.Text = "Correo";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnConexionBD
            // 
            this.btnConexionBD.Location = new System.Drawing.Point(290, 133);
            this.btnConexionBD.Name = "btnConexionBD";
            this.btnConexionBD.Size = new System.Drawing.Size(78, 23);
            this.btnConexionBD.TabIndex = 33;
            this.btnConexionBD.Text = "Conexion BD";
            this.btnConexionBD.UseVisualStyleBackColor = true;
            this.btnConexionBD.Click += new System.EventHandler(this.btnConexionBD_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 393);
            this.Controls.Add(this.bntCorreo);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.bntCorreo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtColor;
        private System.Windows.Forms.TextBox txtCantidadPatas;
        private System.Windows.Forms.TextBox txtGenero;
        private System.Windows.Forms.TextBox txtRaza;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCorrer;
        private System.Windows.Forms.Button btnDormir;
        private System.Windows.Forms.Button btnSentarse;
        private System.Windows.Forms.Button btnLadrar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnValidarID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox bntCorreo;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnValidarUrl;
        private System.Windows.Forms.Button btnNumeroTelefonico;
        private System.Windows.Forms.Button btnFlowLayout;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnCrud;
        private System.Windows.Forms.Button checkBList;
        private System.Windows.Forms.Button btnCBoxCarros;
        private System.Windows.Forms.Button btnJuegoLabels;
        private System.Windows.Forms.Button btnFecha;
        private System.Windows.Forms.Button btnLinkLabel;
        private System.Windows.Forms.Button btnConexionBD;
    }
}

