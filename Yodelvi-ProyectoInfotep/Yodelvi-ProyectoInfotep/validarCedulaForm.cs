﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Yodelvi_ProyectoInfotep
{
    public partial class validarCedulaForm : Form
    {
        public validarCedulaForm()
        {
            InitializeComponent();
        }

        private void txtCedula_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) 
            {
                string contenidoTxt = txtCedula.Text;
                string expresionRegular = @"\A([0-9]{3})(\s?)(\-?)(\s?)([0-9]{7})(\s?)(\-?)(\s?)([0-9])\Z";

                Regex validadorRE = new Regex(expresionRegular);

                if (validadorRE.IsMatch(contenidoTxt))
                {
                    MessageBox.Show("La cedula es correcta", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

                else 
                {
                    MessageBox.Show("La cedula es incorrecta \n\nPatron esperado: 000-0000000-0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }


            }
        }

        private void txtCedula_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
