﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yodelvi_ProyectoInfotep.clases;

namespace Yodelvi_ProyectoInfotep
{
    public partial class formCrudDGV : Form
    {


        private List<Persona> personas = new List<Persona>() {
            new Persona{ ID = 1, Nombre = "Yodelvi", Apellido="Marten", Basketball = true, Cine = false},
            new Persona{ ID = 2, Nombre = "Jesus", Apellido="Taveras", Basketball = false, Cine = true},
            new Persona{ ID = 3, Nombre = "pedro", Apellido="casas", Basketball = false, Cine = false},
            new Persona{ ID = 4, Nombre = "maria", Apellido="solis", Basketball = true, Cine = true},
        };

        private Persona persona = new Persona();

        // Metodos referentes a agregar nuevas personas a la aplicacion

        public void asignarDatosPersona() 
        {
            this.persona.Nombre = this.txtNombre.Text;
            this.persona.Apellido = this.txtApellido.Text;
            this.persona.Cine = this.checkCine.Checked;
            this.persona.Basketball = this.checkBasketball.Checked;
        }

        public bool validar() 
        {
            bool formularioValido = true;

            if (this.persona.Nombre == string.Empty) 
            {
                MessageBox.Show("El campo nombre no puede estar vacio", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.txtNombre.Focus();
                formularioValido = false;
            }

            if (this.persona.Apellido == string.Empty)
            {
                MessageBox.Show("El campo apellido no puede estar vacio", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.txtApellido.Focus();
                formularioValido = false;
            }

            return formularioValido;
        }

        public void agregar() 
        {
            this.asignarDatosPersona();
            if (validar()) 
            { 
                int nuevoID = this.personas.Max(x => x.ID) + 1;
                this.persona.ID = nuevoID;

                //agregar nueva persona a la lista
                personas.Add(persona);

                //limpiar campos y actualizar datagridview
                llenarDgv();
                limpiar();
                

            }

        }

        public void eliminar() 
        {
            if (MessageBox.Show("Esta seguro de eliminar el registro " + this.persona.Nombre.ToUpper() + "?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                this.personas.Remove(this.persona);
                limpiar();
                llenarDgv();
            }
        }

        public void editar() 
        {
            asignarDatosPersona();
            if (validar()) 
            {
                var aux = this.personas.ToDictionary(x => x.ID, x => x);
                aux[this.persona.ID] = this.persona;
                this.personas = aux.Select(x => x.Value).ToList();
                limpiar();
                llenarDgv();
            }
        
        }

        public void limpiar() 
        {
            persona = new Persona();
            this.asignarDatoVista();
        }

        public void asignarDatoVista() 
        {
            this.txtNombre.Text = this.persona.Nombre;
            this.txtApellido.Text = this.persona.Apellido;
            this.checkBasketball.Checked = this.persona.Basketball;
            this.checkCine.Checked = this.persona.Cine;
        }



        public void llenarDgv() 
        {
            this.dgvPersonas.DataSource = personas.ToList();
        }

        public void buscar() 
        {
            var busqueda = this.personas.Where(x => x.Nombre.ToLower().Contains(this.txtBuscar.Text)).ToList(); 
    
            this.dgvPersonas.DataSource = busqueda;
            
        }

        public formCrudDGV()
        {
            InitializeComponent();
            llenarDgv();
            btnCancelar.Enabled = false;
            btnEliminar.Enabled = false;
            btnAceptar.Enabled = false;

        }

        private void txtBuscar_KeyUp(object sender, KeyEventArgs e)
        {
            buscar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            this.agregar();
        }

        private void dgvPersonas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnAgregar.Enabled = false;
            btnCancelar.Enabled = true;
            btnEliminar.Enabled = true;
            btnAceptar.Enabled = true;
            this.persona = (Persona)this.dgvPersonas.CurrentRow.DataBoundItem;
            this.asignarDatoVista();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            btnAgregar.Enabled = true;
            limpiar();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            editar();
        }
    }
}
