﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yodelvi_ProyectoInfotep.clases;

namespace Yodelvi_ProyectoInfotep
{
    public partial class FormCBoxCarros : Form
    {
        public List<Vehiculo> listaVehiculos = new List<Vehiculo>()
            {
                new Vehiculo{ID =0, marcaModelo ="-----"},
                new Vehiculo{ID =1, marcaModelo ="Hyundai Sonata"},
                new Vehiculo{ID =2, marcaModelo ="Hyundai Y20"},
                new Vehiculo{ID =3, marcaModelo ="Kia K5"},
                new Vehiculo{ID =4, marcaModelo ="Honda Civic"},
                new Vehiculo{ID =5, marcaModelo ="Honda Accord"},
                new Vehiculo{ID =6, marcaModelo ="Honda Fit"},
                new Vehiculo{ID =7, marcaModelo ="Honda CRV"},
                new Vehiculo{ID =8, marcaModelo ="Toyota Corolla"},
                new Vehiculo{ID =9, marcaModelo ="Toyota Camry"},
                new Vehiculo{ID =10, marcaModelo ="Toyota Runner"},
                new Vehiculo{ID =11, marcaModelo ="Toyota Rav4"},
                new Vehiculo{ID =12, marcaModelo ="Suzuki Swift"},
                new Vehiculo{ID =13, marcaModelo ="Mitsubishi Montero"},
                new Vehiculo{ID =14, marcaModelo ="Chevrolet Tahoe"},
                new Vehiculo{ID =15, marcaModelo ="Ford Fiesta"},
                new Vehiculo{ID =16, marcaModelo ="Jeep Gran Cherokee"},
                new Vehiculo{ID =17, marcaModelo ="Mazda Mazda5"},
                new Vehiculo{ID =18, marcaModelo ="Ford F-150"},
                new Vehiculo{ID =19, marcaModelo ="Nissan Pathfinder"},
                new Vehiculo{ID =20, marcaModelo ="Subaru WRX STI"}

            };

        public FormCBoxCarros()
        {
            InitializeComponent();
            this.cBoxCarros.DataSource = this.listaVehiculos;
            this.cBoxCarros.DisplayMember = "marcaModelo";
            this.cBoxCarros.ValueMember = "ID";
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            var vehiculo = (Vehiculo)this.cBoxCarros.SelectedItem;
            if (vehiculo.ID >  0 )
            {
                string mensaje = "Vehiculo elegido: \n" + "ID: " + vehiculo.ID.ToString() + " -> " + vehiculo.marcaModelo.ToString();
                MessageBox.Show(mensaje, "info", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Debe elegir una opcion del ComboBox", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
