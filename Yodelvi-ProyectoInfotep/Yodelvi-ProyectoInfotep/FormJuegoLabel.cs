﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep
{
    public partial class FormJuegoLabel : Form
    {
        public FormJuegoLabel()
        {
            InitializeComponent();
        }


        private void lblA_Click_1(object sender, EventArgs e)
        {
            lblA.Visible = false;
        }

        private void lblB_Click(object sender, EventArgs e)
        {
            lblB.Visible = false;
        }
        private void aToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lblB.Visible = false;
            
        }

        private void ocultarAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lblA.Visible = false;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            lblA.Visible = true;
            lblB.Visible = true;
        }

        
    }
}
