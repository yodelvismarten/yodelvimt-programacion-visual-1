﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep
{
    public partial class FormDateTimePicker : Form
    {
        public FormDateTimePicker()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var fechaActual = dateTimePicker1.Value;
            var fecha2meses = fechaActual.AddMonths(2);
            dateTimePicker2.Value = fecha2meses; 
        }
    }
}
