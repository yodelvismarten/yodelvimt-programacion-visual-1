﻿namespace Yodelvi_ProyectoInfotep
{
    partial class formFlowLayout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.deArribaAAbajoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deAbajoAArribaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deIzquierdaADerechaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deDerechaAIzquierdaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deArribaAAbajoToolStripMenuItem,
            this.deAbajoAArribaToolStripMenuItem,
            this.deIzquierdaADerechaToolStripMenuItem,
            this.deDerechaAIzquierdaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(549, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // deArribaAAbajoToolStripMenuItem
            // 
            this.deArribaAAbajoToolStripMenuItem.Name = "deArribaAAbajoToolStripMenuItem";
            this.deArribaAAbajoToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.deArribaAAbajoToolStripMenuItem.Text = "De Arriba a Abajo";
            this.deArribaAAbajoToolStripMenuItem.Click += new System.EventHandler(this.deArribaAAbajoToolStripMenuItem_Click);
            // 
            // deAbajoAArribaToolStripMenuItem
            // 
            this.deAbajoAArribaToolStripMenuItem.Name = "deAbajoAArribaToolStripMenuItem";
            this.deAbajoAArribaToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.deAbajoAArribaToolStripMenuItem.Text = "De Abajo a Arriba";
            this.deAbajoAArribaToolStripMenuItem.Click += new System.EventHandler(this.deAbajoAArribaToolStripMenuItem_Click);
            // 
            // deIzquierdaADerechaToolStripMenuItem
            // 
            this.deIzquierdaADerechaToolStripMenuItem.Name = "deIzquierdaADerechaToolStripMenuItem";
            this.deIzquierdaADerechaToolStripMenuItem.Size = new System.Drawing.Size(139, 20);
            this.deIzquierdaADerechaToolStripMenuItem.Text = "De Izquierda a Derecha";
            this.deIzquierdaADerechaToolStripMenuItem.Click += new System.EventHandler(this.deIzquierdaADerechaToolStripMenuItem_Click);
            // 
            // deDerechaAIzquierdaToolStripMenuItem
            // 
            this.deDerechaAIzquierdaToolStripMenuItem.Name = "deDerechaAIzquierdaToolStripMenuItem";
            this.deDerechaAIzquierdaToolStripMenuItem.Size = new System.Drawing.Size(141, 20);
            this.deDerechaAIzquierdaToolStripMenuItem.Text = "De Derecha A Izquierda";
            this.deDerechaAIzquierdaToolStripMenuItem.Click += new System.EventHandler(this.deDerechaAIzquierdaToolStripMenuItem_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.button2);
            this.flowLayoutPanel1.Controls.Add(this.button3);
            this.flowLayoutPanel1.Controls.Add(this.button4);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Microsoft YaHei", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(549, 458);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Lime;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 107);
            this.button1.TabIndex = 0;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Blue;
            this.button2.Location = new System.Drawing.Point(140, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(131, 107);
            this.button2.TabIndex = 1;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Red;
            this.button3.Location = new System.Drawing.Point(277, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(131, 107);
            this.button3.TabIndex = 2;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Yellow;
            this.button4.Location = new System.Drawing.Point(414, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(131, 107);
            this.button4.TabIndex = 3;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // formFlowLayout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 482);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "formFlowLayout";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "formFlowLayout";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deArribaAAbajoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deAbajoAArribaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deIzquierdaADerechaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deDerechaAIzquierdaToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}