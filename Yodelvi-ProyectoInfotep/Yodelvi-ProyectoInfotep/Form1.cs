﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep
{
    public partial class Form1 : Form
    {
        Perro Miperro = new Perro();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void btnCorrer_Click(object sender, EventArgs e)
        {
            Miperro.correr();
        }

        private void btnLadrar_Click(object sender, EventArgs e)
        {
            Miperro.ladrar();
        }

        private void btnDormir_Click(object sender, EventArgs e)
        {
            Miperro.dormir();
        }

        private void btnSentarse_Click(object sender, EventArgs e)
        {
            Miperro.sentarse();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 abrirForm2 = new Form2();
            abrirForm2.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            form3 abrirForm3 = new form3();
            abrirForm3.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CalculadoraForm abrirCalculadora = new CalculadoraForm();
            abrirCalculadora.Show();
        }

        private void btnValidarID_Click(object sender, EventArgs e)
        {
            validarCedulaForm mostrarFormValidarID = new validarCedulaForm();
            mostrarFormValidarID.Show();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            formValidarCorreo abrirFormCorreo = new formValidarCorreo();
            abrirFormCorreo.Show();
        }

        private void btnValidarUrl_Click(object sender, EventArgs e)
        {
            formValidarURL abrirFormUrl = new formValidarURL();
            abrirFormUrl.Show();
        }

        private void btnNumeroTelefonico_Click(object sender, EventArgs e)
        {
            formValidarNumeroTelefono abrirFormValidarTelefono = new formValidarNumeroTelefono();
            abrirFormValidarTelefono.Show();
        }

        private void btnFlowLayout_Click(object sender, EventArgs e)
        {
            formFlowLayout abrirFormFlowLayout = new formFlowLayout();
            abrirFormFlowLayout.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            formTableLayout abrirFormTableLayout = new formTableLayout();
            abrirFormTableLayout.Show();
        }

        private void btnCrud_Click(object sender, EventArgs e)
        {
            formCrudDGV abrirFormCrud = new formCrudDGV();
            abrirFormCrud.Show();
        }

        private void checkBList_Click(object sender, EventArgs e)
        {
            formCheckBoxList abrirFormCheckBoxList = new formCheckBoxList();
            abrirFormCheckBoxList.Show();
        }

        private void btnCBoxCarros_Click(object sender, EventArgs e)
        {
            new FormCBoxCarros().Show();
        }

        private void btnJuegoLabels_Click(object sender, EventArgs e)
        {
            new FormJuegoLabel().Show();
        }

        private void btnFecha_Click(object sender, EventArgs e)
        {
            new FormDateTimePicker().Show();
        }

        private void btnLinkLabel_Click(object sender, EventArgs e)
        {
            new FormLinkLabel().Show();
        }

        private void btnConexionBD_Click(object sender, EventArgs e)
        {
            new FormAgregarDatosBD().Show();
        }
    }
}
