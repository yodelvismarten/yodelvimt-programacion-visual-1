﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Yodelvi_ProyectoInfotep
{
    public partial class formValidarURL : Form
    {
        public formValidarURL()
        {
            InitializeComponent();
        }

        private void txtURL_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) 
            {
                string contenidoTxt = txtURL.Text;
                string expresionRegular = @"\A(http://|https://)?((w){3})(\.)(\w+)(\.)(com|org|edu|net)\Z";

                Regex validadorURL = new Regex(expresionRegular);
                if (validadorURL.IsMatch(contenidoTxt)) 
                {
                    MessageBox.Show("La URL es correcta", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("La URL es incorrecta", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

            }
        }
    }
}
