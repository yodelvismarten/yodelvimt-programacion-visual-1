﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep
{
    public partial class form3 : Form
    {
        public form3()
        {
            InitializeComponent();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MessageBox.Show("Ha presionado el boton enter","Evento KeyUp", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else if (e.KeyCode == Keys.Back)
            {
                MessageBox.Show("Ha presionado el boton back", "Evento KeyUp", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else if (e.KeyCode == Keys.Delete)
            {
                MessageBox.Show("Ha presionado el boton delete", "Evento KeyUp", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }
    }
}
