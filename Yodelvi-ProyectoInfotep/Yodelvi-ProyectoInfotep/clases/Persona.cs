﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yodelvi_ProyectoInfotep.clases
{
    public class Persona
    {
        private int id;
        private string nombre;
        private string apellido;

        public int ID { set { this.id = value; } get { return this.id; } }

        public string Nombre { set { this.nombre = value; } get { return this.nombre; } }

        public string Apellido { set { this.apellido = value; } get { return this.apellido; } }

        public bool Cine { get; set; }

        public bool Basketball { get; set; }
    }
}
