﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep.clases
{
    public class ConexionBD
    {
        string connectionString = 
            "datasource = 127.0.0.1; " +
            "port = 3306; " +
            "username = root; " +
            "password =; " +
            "database = Empresa;";

        public MySqlConnection conectarBD() {
            MySqlConnection conexion = new MySqlConnection(this.connectionString);
            conexion.Open();
            return conexion;
        }

        public void enviarQuery(string consulta) {
            MySqlCommand query = conectarBD().CreateCommand();
            query.CommandText = consulta;
            query.ExecuteNonQuery();
            
        }
    }
}
