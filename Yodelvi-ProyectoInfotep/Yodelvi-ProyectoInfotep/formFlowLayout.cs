﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep
{
    public partial class formFlowLayout : Form
    {
        public formFlowLayout()
        {
            InitializeComponent();
        }

        private void deArribaAAbajoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.FlowDirection = FlowDirection.TopDown;
        }

        private void deAbajoAArribaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.FlowDirection = FlowDirection.BottomUp;
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void deIzquierdaADerechaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.FlowDirection = FlowDirection.LeftToRight;
        }

        private void deDerechaAIzquierdaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.FlowDirection = FlowDirection.RightToLeft;
        }
    }
}
