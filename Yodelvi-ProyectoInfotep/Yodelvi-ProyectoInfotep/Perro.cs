﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yodelvi_ProyectoInfotep
{
    class Perro
    {
        public string color {
            set;
            get;
        }
        public int cantidadPatas{
            set;
            get;
        }
        public string genero{
            set;
            get;
        }
        public string raza {
            set;
            get;
        }

        public void correr() {
            MessageBox.Show("El perro esta corriendo", "Aviso");        
        }

        public void ladrar()
        {
            MessageBox.Show("El perro esta ladrando", "Aviso");
        }

        public void sentarse()
        {
            MessageBox.Show("El perro esta sentado", "Aviso");
        }

        public void dormir()
        {
            MessageBox.Show("El perro esta durmiendo", "Aviso");
        }



    }
}
