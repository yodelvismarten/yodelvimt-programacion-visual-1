﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasUsuarios
{  

    public partial class RegistrarUsuario : Form
    {
        AdministrarUsuarios administrarUsuarios = new AdministrarUsuarios();
        AdministradorPosiciones administradorPosiciones = new AdministradorPosiciones();

        public RegistrarUsuario()
        {
            InitializeComponent();
            llenarComboBPosiciones();
        }

        public void llenarComboBPosiciones()
        {
            administradorPosiciones.listarPosiciones();
            ComboBPosicion.DataSource = administradorPosiciones.listaPosiciones;
            ComboBPosicion.DisplayMember = "nombre";
            ComboBPosicion.ValueMember = "ID";
        }


        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            administrarUsuarios.asignarDatosObjeto(
                txtUsername.Text, txtPassword.Text, txtnombre.Text, txtApellido.Text, txtCedula.Text,
                txtDireccion.Text, txtTelefono.Text, Convert.ToInt32(ComboBPosicion.SelectedValue), txtEmail.Text,
                checkBEsAdministrador.Checked, checkBEstado.Checked);
            administrarUsuarios.registrarUsuario();
        }
    }
}
