﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasUsuarios
{
    public partial class EditarUsuario : Form
    {
        AdministradorPosiciones administradorPosiciones = new AdministradorPosiciones();
        AdministrarUsuarios administrarUsuarios = new AdministrarUsuarios();
        int id;

        public EditarUsuario()
        {
            InitializeComponent();
            llenarComboBPosiciones();
        }

        public void llenarComboBPosiciones()
        {
            administradorPosiciones.listarPosiciones();
            ComboBPosicion.DataSource = administradorPosiciones.listaPosiciones;
            ComboBPosicion.DisplayMember = "nombre";
            ComboBPosicion.ValueMember = "ID";
        }

        public void llenarFormulario(AdministrarUsuarios administrarUsuarios)
        {
            this.id = administrarUsuarios.usuario.ID;
            this.txtnombre.Text = administrarUsuarios.usuario.nombre;
            this.txtApellido.Text = administrarUsuarios.usuario.apellido;
            this.txtCedula.Text = administrarUsuarios.usuario.cedula;
            this.txtDireccion.Text = administrarUsuarios.usuario.direccion;
            this.txtEmail.Text = administrarUsuarios.usuario.email;
            this.txtTelefono.Text = administrarUsuarios.usuario.telefono;
            this.txtUsername.Text = administrarUsuarios.usuario.username;
            this.txtPassword.Text = administrarUsuarios.usuario.password;
            this.ComboBPosicion.SelectedValue = administrarUsuarios.usuario.idPosicion;
            this.checkBEsAdministrador.Checked = administrarUsuarios.usuario.esAdministrador;
            this.checkBEstado.Checked = administrarUsuarios.usuario.estado;
        }

        public void editarObjetoUsuario()
        {
            administrarUsuarios.asignarDatosObjeto(
                txtUsername.Text, txtPassword.Text, txtnombre.Text, txtApellido.Text, txtCedula.Text,
                txtDireccion.Text, txtTelefono.Text, Convert.ToInt32(ComboBPosicion.SelectedValue), txtEmail.Text,
                checkBEsAdministrador.Checked, checkBEstado.Checked);
        }
        
        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            editarObjetoUsuario();
            administrarUsuarios.editarUsuario(this.id);
        }
    }
}
