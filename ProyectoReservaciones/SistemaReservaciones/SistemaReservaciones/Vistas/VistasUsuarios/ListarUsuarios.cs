﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.Vistas.VistasPosiciones;
using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;
using SistemaReservaciones.LogicaNegocio.Clases.Entidades;

namespace SistemaReservaciones.Vistas.VistasUsuarios
{
    public partial class ListarUsuarios : Form
    {
        AdministrarUsuarios administrarUsuarios = new AdministrarUsuarios();

        public ListarUsuarios()
        {
            InitializeComponent();
            llenarDgv();
        }

        public void llenarDgv()
        {
            administrarUsuarios.listarUsuarios();
            dgvUsuarios.DataSource = administrarUsuarios.listaUsuarios;
            this.dgvUsuarios.Columns["ID"].Visible = false;
            this.dgvUsuarios.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvUsuarios.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void btnRegistrarUsuario_Click(object sender, EventArgs e)
        {
            new RegistrarUsuario().Show();
        }

        private void btnEditarUsuario_Click(object sender, EventArgs e)
        {
            EditarUsuario editarUsuario = new EditarUsuario();
            administrarUsuarios.usuario  = (Usuario)dgvUsuarios.CurrentRow.DataBoundItem;
            editarUsuario.llenarFormulario(administrarUsuarios);
            editarUsuario.Show();
        }

        private void btnAbrirFormPosiciones_Click(object sender, EventArgs e)
        {
            new ListarPosiciones().Show();
        }

        private void btnEliminarUsuario_Click(object sender, EventArgs e)
        {
            administrarUsuarios.usuario = (Usuario)dgvUsuarios.CurrentRow.DataBoundItem;
            string registro = $"{administrarUsuarios.usuario.nombre} {administrarUsuarios.usuario.apellido}";

            DialogResult confirmacionEliminacion = MessageBox.Show($@"Desea eliminar el registro {registro}", "Aviso",
                                                                   MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmacionEliminacion == DialogResult.Yes)
            {
                administrarUsuarios.eliminarUsuario(administrarUsuarios.usuario.ID);
            }
        }
    }
}
