﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasCategoriaHabitaciones
{
    public partial class RegistrarCategoriaHabitacion : Form
    {
        //objeto de tipo AdministradorCategoriaHabitaciones para utilizar sus metodos
        AdministradorCategoriaHabitaciones objCategoriaHabitaciones = new AdministradorCategoriaHabitaciones();

        public RegistrarCategoriaHabitacion()
        {
            InitializeComponent();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            objCategoriaHabitaciones.asignarDatosObjeto(this.txtNombreCategoria.Text, this.checkEstadoCategoria);
            objCategoriaHabitaciones.registrarCategoria();
        }
    }
}
