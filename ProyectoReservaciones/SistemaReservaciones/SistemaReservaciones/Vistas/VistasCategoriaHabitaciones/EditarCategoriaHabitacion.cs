﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;
using SistemaReservaciones.Vistas.VistasCategoriaHabitaciones;

namespace SistemaReservaciones.Vistas.VistasCategoriaHabitaciones
{
    public partial class EditarCategoriaHabitacion : Form
    {
        AdministradorCategoriaHabitaciones administradorCategoriaHabitaciones = new AdministradorCategoriaHabitaciones();
        public int id;

        public EditarCategoriaHabitacion()
        {
            InitializeComponent();
        }

        public void LlenarFormulario(AdministradorCategoriaHabitaciones objCategoriaHabitaciones)
        {
            this.id = objCategoriaHabitaciones.categoriaHabitacion.ID;
            this.txtNombre.Text = objCategoriaHabitaciones.categoriaHabitacion.nombre;
            this.checkBEstado.Checked = objCategoriaHabitaciones.categoriaHabitacion.estado;
        }

        public void editarObjetoCategoria()
        {
            administradorCategoriaHabitaciones.asignarDatosObjeto(this.txtNombre.Text, this.checkBEstado);
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            this.editarObjetoCategoria();
            administradorCategoriaHabitaciones.editarCategoria(id);
            new ListarCategoriasHabitaciones().llenardgv();
        }
    }
}
