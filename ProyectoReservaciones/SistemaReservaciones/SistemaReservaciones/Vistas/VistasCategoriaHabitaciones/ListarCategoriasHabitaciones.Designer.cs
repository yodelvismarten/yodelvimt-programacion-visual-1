﻿namespace SistemaReservaciones.Vistas.VistasCategoriaHabitaciones
{
    partial class ListarCategoriasHabitaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelContenedorHuesped = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnEliminarCategoriaHabitacion = new System.Windows.Forms.Button();
            this.btnRegistrarCategoriaHabitacion = new System.Windows.Forms.Button();
            this.btnEditarCategoriaHabitacion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvCategoriasHabitaciones = new System.Windows.Forms.DataGridView();
            this.panelContenedorHuesped.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategoriasHabitaciones)).BeginInit();
            this.SuspendLayout();
            // 
            // panelContenedorHuesped
            // 
            this.panelContenedorHuesped.Controls.Add(this.groupBox1);
            this.panelContenedorHuesped.Controls.Add(this.label1);
            this.panelContenedorHuesped.Controls.Add(this.textBox1);
            this.panelContenedorHuesped.Controls.Add(this.label10);
            this.panelContenedorHuesped.Controls.Add(this.dgvCategoriasHabitaciones);
            this.panelContenedorHuesped.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenedorHuesped.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelContenedorHuesped.Location = new System.Drawing.Point(0, 0);
            this.panelContenedorHuesped.Name = "panelContenedorHuesped";
            this.panelContenedorHuesped.Size = new System.Drawing.Size(800, 526);
            this.panelContenedorHuesped.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnEliminarCategoriaHabitacion);
            this.groupBox1.Controls.Add(this.btnRegistrarCategoriaHabitacion);
            this.groupBox1.Controls.Add(this.btnEditarCategoriaHabitacion);
            this.groupBox1.Location = new System.Drawing.Point(12, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(770, 84);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // btnEliminarCategoriaHabitacion
            // 
            this.btnEliminarCategoriaHabitacion.BackColor = System.Drawing.Color.Red;
            this.btnEliminarCategoriaHabitacion.FlatAppearance.BorderSize = 0;
            this.btnEliminarCategoriaHabitacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarCategoriaHabitacion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarCategoriaHabitacion.ForeColor = System.Drawing.Color.White;
            this.btnEliminarCategoriaHabitacion.Location = new System.Drawing.Point(679, 33);
            this.btnEliminarCategoriaHabitacion.Name = "btnEliminarCategoriaHabitacion";
            this.btnEliminarCategoriaHabitacion.Size = new System.Drawing.Size(85, 30);
            this.btnEliminarCategoriaHabitacion.TabIndex = 5;
            this.btnEliminarCategoriaHabitacion.Text = "Eliminar";
            this.btnEliminarCategoriaHabitacion.UseVisualStyleBackColor = false;
            this.btnEliminarCategoriaHabitacion.Click += new System.EventHandler(this.btnEliminarCategoriaHabitacion_Click);
            // 
            // btnRegistrarCategoriaHabitacion
            // 
            this.btnRegistrarCategoriaHabitacion.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnRegistrarCategoriaHabitacion.FlatAppearance.BorderSize = 0;
            this.btnRegistrarCategoriaHabitacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistrarCategoriaHabitacion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarCategoriaHabitacion.ForeColor = System.Drawing.Color.White;
            this.btnRegistrarCategoriaHabitacion.Location = new System.Drawing.Point(497, 33);
            this.btnRegistrarCategoriaHabitacion.Name = "btnRegistrarCategoriaHabitacion";
            this.btnRegistrarCategoriaHabitacion.Size = new System.Drawing.Size(85, 30);
            this.btnRegistrarCategoriaHabitacion.TabIndex = 3;
            this.btnRegistrarCategoriaHabitacion.Text = "Registrar";
            this.btnRegistrarCategoriaHabitacion.UseVisualStyleBackColor = false;
            this.btnRegistrarCategoriaHabitacion.Click += new System.EventHandler(this.btnRegistrarCategoriaHabitacion_Click);
            // 
            // btnEditarCategoriaHabitacion
            // 
            this.btnEditarCategoriaHabitacion.BackColor = System.Drawing.Color.DarkOrange;
            this.btnEditarCategoriaHabitacion.FlatAppearance.BorderSize = 0;
            this.btnEditarCategoriaHabitacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarCategoriaHabitacion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarCategoriaHabitacion.ForeColor = System.Drawing.Color.White;
            this.btnEditarCategoriaHabitacion.Location = new System.Drawing.Point(588, 33);
            this.btnEditarCategoriaHabitacion.Name = "btnEditarCategoriaHabitacion";
            this.btnEditarCategoriaHabitacion.Size = new System.Drawing.Size(85, 30);
            this.btnEditarCategoriaHabitacion.TabIndex = 4;
            this.btnEditarCategoriaHabitacion.Text = "Editar";
            this.btnEditarCategoriaHabitacion.UseVisualStyleBackColor = false;
            this.btnEditarCategoriaHabitacion.Click += new System.EventHandler(this.btnEditarCategoriaHabitacion_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(800, 59);
            this.label1.TabIndex = 6;
            this.label1.Text = "Listado Categorias";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Bahnschrift Light", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(12, 160);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(770, 21);
            this.textBox1.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 141);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "Buscar";
            // 
            // dgvCategoriasHabitaciones
            // 
            this.dgvCategoriasHabitaciones.AllowUserToAddRows = false;
            this.dgvCategoriasHabitaciones.AllowUserToDeleteRows = false;
            this.dgvCategoriasHabitaciones.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvCategoriasHabitaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCategoriasHabitaciones.Location = new System.Drawing.Point(12, 187);
            this.dgvCategoriasHabitaciones.Name = "dgvCategoriasHabitaciones";
            this.dgvCategoriasHabitaciones.ReadOnly = true;
            this.dgvCategoriasHabitaciones.Size = new System.Drawing.Size(770, 327);
            this.dgvCategoriasHabitaciones.TabIndex = 0;
            // 
            // ListarCategoriasHabitaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 526);
            this.Controls.Add(this.panelContenedorHuesped);
            this.Name = "ListarCategoriasHabitaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ListarCategoriasHabitaciones";
            this.panelContenedorHuesped.ResumeLayout(false);
            this.panelContenedorHuesped.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategoriasHabitaciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelContenedorHuesped;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEliminarCategoriaHabitacion;
        private System.Windows.Forms.Button btnEditarCategoriaHabitacion;
        private System.Windows.Forms.Button btnRegistrarCategoriaHabitacion;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvCategoriasHabitaciones;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}