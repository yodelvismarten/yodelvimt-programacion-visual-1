﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.Entidades;
using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasCategoriaHabitaciones
{
    public partial class ListarCategoriasHabitaciones : Form
    {
        //objeto de tipo AdministradorCategoriaHabitaciones para utilizar sus metodos
        public AdministradorCategoriaHabitaciones objCategoriaHabitaciones = new AdministradorCategoriaHabitaciones();

        public ListarCategoriasHabitaciones()
        {
            InitializeComponent();
            llenardgv();
        }

        public void llenardgv()
        {
            this.objCategoriaHabitaciones.listarCategorias();
            this.dgvCategoriasHabitaciones.DataSource = objCategoriaHabitaciones.listaCategorias;
            this.dgvCategoriasHabitaciones.Columns["ID"].Visible = false;
            this.dgvCategoriasHabitaciones.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvCategoriasHabitaciones.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            //this.dgvCategoriasHabitaciones.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void btnRegistrarCategoriaHabitacion_Click(object sender, EventArgs e)
        {
            new RegistrarCategoriaHabitacion().Show();
        }

        private void btnEditarCategoriaHabitacion_Click(object sender, EventArgs e)
        {
            objCategoriaHabitaciones.categoriaHabitacion = (CategoriaHabitacion)dgvCategoriasHabitaciones.CurrentRow.DataBoundItem;
            EditarCategoriaHabitacion objEditarCategoriaHabitacion = new EditarCategoriaHabitacion();
            objEditarCategoriaHabitacion.LlenarFormulario(objCategoriaHabitaciones);
            objEditarCategoriaHabitacion.Show();

        }

        private void btnEliminarCategoriaHabitacion_Click(object sender, EventArgs e)
        {
            objCategoriaHabitaciones.categoriaHabitacion = (CategoriaHabitacion)dgvCategoriasHabitaciones.CurrentRow.DataBoundItem;
            DialogResult confirmarEliminacion = MessageBox.Show(
                $"Desea eliminar el registro {objCategoriaHabitaciones.categoriaHabitacion.nombre} ?",
                "Aviso", 
                MessageBoxButtons.YesNo, 
                MessageBoxIcon.Warning);

            //si la respuesta del dialog result es Yes, se llama a la funcion eliminar del AdministradosCategoriasHabitaciones y 
            //se le pasa el id para su eliminacion logica
            if (confirmarEliminacion == DialogResult.Yes)
            {
                objCategoriaHabitaciones.eliminarCategoria(objCategoriaHabitaciones.categoriaHabitacion.ID);
            }
        }
    }
}
