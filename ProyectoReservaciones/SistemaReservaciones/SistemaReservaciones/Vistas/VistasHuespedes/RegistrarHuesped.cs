﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//IMPORTACIONES DEL DESARROLLADOR
using SistemaReservaciones.LogicaNegocio.Clases;
using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasHuespedes
{
    public partial class RegistrarHuesped : Form
    {
        public RegistrarHuesped()
        {
            InitializeComponent();
            this.llenarComboBDocumento();
        }

        //LLenar ComboBox de tipo de documento
        public void llenarComboBDocumento() {
            this.comboBDocumentoHuesped.Items.Add("Cedula");
            this.comboBDocumentoHuesped.Items.Add("Pasaporte");
        }

        
        //Objeto global de la clase 'AdministradorHuespedes' para utilizar en todos los metodos en los que sea necesario
        AdministradorHuespedes administradorHuespedes = new AdministradorHuespedes();

        public void registrarHuesped()
        {

            administradorHuespedes.asignarDatosObjeto(
                this.txtNombreHuesped.Text,
                this.txtApellidoHuesped.Text,
                this.comboBDocumentoHuesped,
                this.txtNumeroDocumentoHuesped.Text,
                this.txtTelefonoHuesped.Text,
                this.checkBoxEstado);
            administradorHuespedes.registrarHuesped();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            this.registrarHuesped();
            this.administradorHuespedes.listarHuespedes();
            this.Close();

        }

    }
}
