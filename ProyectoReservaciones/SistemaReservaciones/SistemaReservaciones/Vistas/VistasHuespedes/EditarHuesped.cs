﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaReservaciones.Vistas.VistasHuespedes;
using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;


namespace SistemaReservaciones.Vistas.VistasHuespedes
{
    public partial class EditarHuesped : Form
    {
        //objeto para utlizar los metodos de la clase 'AdministradorHuespedes'
        AdministradorHuespedes administradorHuespedes = new AdministradorHuespedes();
        public int id;
        
        public EditarHuesped()
        {
            InitializeComponent();
            llenarComboBDocumento();
        }

        //LLenar ComboBox de tipo de documento
        public void llenarComboBDocumento()
        {
            this.comboBDocumentoHuesped.Items.Add("Cedula");
            this.comboBDocumentoHuesped.Items.Add("Pasaporte");
        }

        //metodo para llenar el formulario con los datos del registro que se desea editar
        public void llenarFormulario(AdministradorHuespedes objHuesped)
        {
            this.id = objHuesped.huesped.ID;
            this.txtNombreHuesped.Text = objHuesped.huesped.nombre;
            this.txtApellidoHuesped.Text = objHuesped.huesped.apellido;
            this.comboBDocumentoHuesped.SelectedItem =objHuesped.huesped.tipoDocumento;
            this.txtNumeroDocumentoHuesped.Text = objHuesped.huesped.numeroDocumento;
            this.txtTelefonoHuesped.Text = objHuesped.huesped.telefono;
            this.checkBoxEstado.Checked = objHuesped.huesped.estado;
        }


        public void editarObjetoHuesped()
        {
            administradorHuespedes.asignarDatosObjeto(
                this.txtNombreHuesped.Text,
                this.txtApellidoHuesped.Text,
                this.comboBDocumentoHuesped,
                this.txtNumeroDocumentoHuesped.Text,
                this.txtTelefonoHuesped.Text,
                this.checkBoxEstado);
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            this.editarObjetoHuesped();
            this.administradorHuespedes.editarHuesped(this.id);
            //metodo para actualizar el DataGridView despues de haber actualizado la informacion. NO ARROJA ERROR, PERO NO FUNCIONA
            //new ListarHuespedes().llenarDGV();

            
            this.Close();
        }
    }
}
