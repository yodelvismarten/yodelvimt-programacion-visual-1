﻿namespace SistemaReservaciones.Vistas.VistasHuespedes
{
    partial class ListarHuespedes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelContenedorHuesped = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEliminarHuesped = new System.Windows.Forms.Button();
            this.btnEditarHuesped = new System.Windows.Forms.Button();
            this.btnRegistrarHuesped = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvHuespedes = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelContenedorHuesped.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHuespedes)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelContenedorHuesped
            // 
            this.panelContenedorHuesped.Controls.Add(this.groupBox1);
            this.panelContenedorHuesped.Controls.Add(this.label1);
            this.panelContenedorHuesped.Controls.Add(this.textBox1);
            this.panelContenedorHuesped.Controls.Add(this.label10);
            this.panelContenedorHuesped.Controls.Add(this.dgvHuespedes);
            this.panelContenedorHuesped.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenedorHuesped.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelContenedorHuesped.Location = new System.Drawing.Point(0, 0);
            this.panelContenedorHuesped.Name = "panelContenedorHuesped";
            this.panelContenedorHuesped.Size = new System.Drawing.Size(794, 526);
            this.panelContenedorHuesped.TabIndex = 3;
            this.panelContenedorHuesped.Paint += new System.Windows.Forms.PaintEventHandler(this.panelContenedorHuesped_Paint);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(794, 59);
            this.label1.TabIndex = 6;
            this.label1.Text = "Listado Huespedes";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnEliminarHuesped
            // 
            this.btnEliminarHuesped.BackColor = System.Drawing.Color.Red;
            this.btnEliminarHuesped.FlatAppearance.BorderSize = 0;
            this.btnEliminarHuesped.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarHuesped.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarHuesped.ForeColor = System.Drawing.Color.White;
            this.btnEliminarHuesped.Location = new System.Drawing.Point(679, 31);
            this.btnEliminarHuesped.Name = "btnEliminarHuesped";
            this.btnEliminarHuesped.Size = new System.Drawing.Size(85, 30);
            this.btnEliminarHuesped.TabIndex = 5;
            this.btnEliminarHuesped.Text = "Eliminar";
            this.btnEliminarHuesped.UseVisualStyleBackColor = false;
            this.btnEliminarHuesped.Click += new System.EventHandler(this.btnEliminarHuesped_Click);
            // 
            // btnEditarHuesped
            // 
            this.btnEditarHuesped.BackColor = System.Drawing.Color.DarkOrange;
            this.btnEditarHuesped.FlatAppearance.BorderSize = 0;
            this.btnEditarHuesped.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarHuesped.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarHuesped.ForeColor = System.Drawing.Color.White;
            this.btnEditarHuesped.Location = new System.Drawing.Point(588, 31);
            this.btnEditarHuesped.Name = "btnEditarHuesped";
            this.btnEditarHuesped.Size = new System.Drawing.Size(85, 30);
            this.btnEditarHuesped.TabIndex = 4;
            this.btnEditarHuesped.Text = "Editar";
            this.btnEditarHuesped.UseVisualStyleBackColor = false;
            this.btnEditarHuesped.Click += new System.EventHandler(this.btnEditarHuesped_Click);
            // 
            // btnRegistrarHuesped
            // 
            this.btnRegistrarHuesped.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnRegistrarHuesped.FlatAppearance.BorderSize = 0;
            this.btnRegistrarHuesped.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistrarHuesped.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarHuesped.ForeColor = System.Drawing.Color.White;
            this.btnRegistrarHuesped.Location = new System.Drawing.Point(497, 31);
            this.btnRegistrarHuesped.Name = "btnRegistrarHuesped";
            this.btnRegistrarHuesped.Size = new System.Drawing.Size(85, 30);
            this.btnRegistrarHuesped.TabIndex = 3;
            this.btnRegistrarHuesped.Text = "Registrar";
            this.btnRegistrarHuesped.UseVisualStyleBackColor = false;
            this.btnRegistrarHuesped.Click += new System.EventHandler(this.btnRegistrarHuesped_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Bahnschrift Light", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(12, 160);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(770, 21);
            this.textBox1.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 141);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "Buscar";
            // 
            // dgvHuespedes
            // 
            this.dgvHuespedes.AllowUserToAddRows = false;
            this.dgvHuespedes.AllowUserToDeleteRows = false;
            this.dgvHuespedes.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvHuespedes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHuespedes.Location = new System.Drawing.Point(12, 187);
            this.dgvHuespedes.Name = "dgvHuespedes";
            this.dgvHuespedes.ReadOnly = true;
            this.dgvHuespedes.Size = new System.Drawing.Size(770, 327);
            this.dgvHuespedes.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnEliminarHuesped);
            this.groupBox1.Controls.Add(this.btnEditarHuesped);
            this.groupBox1.Controls.Add(this.btnRegistrarHuesped);
            this.groupBox1.Location = new System.Drawing.Point(12, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(770, 84);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // ListarHuespedes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 526);
            this.Controls.Add(this.panelContenedorHuesped);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ListarHuespedes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ListarHuespedes";
            this.panelContenedorHuesped.ResumeLayout(false);
            this.panelContenedorHuesped.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHuespedes)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelContenedorHuesped;
        private System.Windows.Forms.Button btnRegistrarHuesped;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvHuespedes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEliminarHuesped;
        private System.Windows.Forms.Button btnEditarHuesped;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}