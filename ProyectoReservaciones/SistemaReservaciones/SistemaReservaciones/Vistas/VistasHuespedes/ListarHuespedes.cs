﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.Entidades;
using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;


namespace SistemaReservaciones.Vistas.VistasHuespedes
{
    public partial class ListarHuespedes : Form
    {

        //Instancia para utilizar los metodos de la clase AdministradorHuespedes
        public AdministradorHuespedes objHuesped = new AdministradorHuespedes();


        public ListarHuespedes()
        {
            InitializeComponent();
            llenarDGV();
        }

        private void btnRegistrarHuesped_Click(object sender, EventArgs e)
        {
            new RegistrarHuesped().Show();
        }

        private void btnEditarHuesped_Click(object sender, EventArgs e)
        {
            // creacion de objeto de tipo huesped con la informacion seleccionada en el data grid view
            objHuesped.huesped = (Huesped)this.dgvHuespedes.CurrentRow.DataBoundItem;

            //objeto del formulario de edicion de huespedes
            EditarHuesped objEditar = new EditarHuesped();
            objEditar.Show();
            //llamada al metodo de llenar formulario para completar los campos de edicion con la informacion seleccionada por el usuario
            objEditar.llenarFormulario(objHuesped);

        }

        //METODO PARA LLENAR EL DATA GRID VIEW
        public void llenarDGV()
        {
            objHuesped.listarHuespedes();
            this.dgvHuespedes.DataSource = this.objHuesped.huespedes.ToList();
            this.dgvHuespedes.Columns["ID"].Visible = false;
            this.dgvHuespedes.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvHuespedes.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

        }

        //METODO PARA REALIZAR LA ELIMINACION LOGICA
        private void btnEliminarHuesped_Click(object sender, EventArgs e)
        {

            Huesped registroSeleccionadoDgv = (Huesped)this.dgvHuespedes.CurrentRow.DataBoundItem;
            string nombre = registroSeleccionadoDgv.nombre.ToString() +" "+ registroSeleccionadoDgv.apellido.ToString();
            DialogResult confirmacionEliminacion = MessageBox.Show($"Desea Eliminar el registro {nombre} ?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmacionEliminacion == DialogResult.Yes)
            {
                objHuesped.eliminarHuesped(registroSeleccionadoDgv.ID);
            }
        }

        private void panelContenedorHuesped_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
