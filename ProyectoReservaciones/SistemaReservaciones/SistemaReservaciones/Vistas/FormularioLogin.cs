﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas
{
    public partial class FormularioLogin : Form
    {
        AdministradorSesion administradorSesion = new AdministradorSesion();
        FormularioInicio formularioInicio = new FormularioInicio();

        public FormularioLogin()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text != string.Empty && txtPassword.Text != string.Empty)
            {
                if (administradorSesion.login(txtUsername.Text, txtPassword.Text))
                {
                    formularioInicio.validarPermisoUsuario(administradorSesion.sesion.esAdministrador);
                    formularioInicio.Show();
                    this.Hide();
                }

                else
                {
                    lblCamposIncorrectos.Text = "Nombre de Usuario o Contaseña Incorrectos";
                    lblCamposIncorrectos.Visible = true;
                }
            }

            else
            {
                lblCamposIncorrectos.Text = "Nombre de Usuario o Contaseña Incorrectos";
                lblCamposIncorrectos.Visible = true;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
