﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.Vistas.VistasHabitaciones;
using SistemaReservaciones.Vistas.VistasCategoriaHabitaciones;
using SistemaReservaciones.LogicaNegocio.Clases.Entidades;
using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;


namespace SistemaReservaciones.Vistas.VistasHabitaciones
{
    public partial class ListarHabitaciones : Form
    {
        AdministradorCategoriaHabitaciones administradorCategoriaHabitaciones = new AdministradorCategoriaHabitaciones();
        AdministradorHabitaciones administradorHabitaciones = new AdministradorHabitaciones();


        public ListarHabitaciones()
        {
            InitializeComponent();
            llenarDGV();
        }

        public void llenarDGV()
        {
            administradorHabitaciones.listarHabitaciones();
            dgvHabitaciones.DataSource = administradorHabitaciones.listaHabitaciones;
            dgvHabitaciones.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvHabitaciones.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvHabitaciones.Columns["ID"].Visible = false;
            
        }

        //metodo para llenar los comobox de los formulario de registrar y editar
        public void llenarComboBoxCategoria(ComboBox categoria)
        {
            administradorCategoriaHabitaciones.listarCategorias();
            categoria.DataSource = administradorCategoriaHabitaciones.listaCategorias;
            categoria.DisplayMember = "nombre";
            categoria.ValueMember = "ID";

        }

        private void btnRegistrarHabitacion_Click(object sender, EventArgs e)
        {
            new RegistrarHabitacion().Show();
        }

        private void btnEditarHabitacion_Click(object sender, EventArgs e)
        {
            administradorHabitaciones.habitacion = (Habitacion)dgvHabitaciones.CurrentRow.DataBoundItem;
            EditarHabitacion editarHabitacion = new EditarHabitacion();
            editarHabitacion.llenarFormulario(administradorHabitaciones);
            editarHabitacion.Show();

        }

        private void btnEliminarHabitacion_Click(object sender, EventArgs e)
        {
            Habitacion registroSeleccionado = (Habitacion) dgvHabitaciones.CurrentRow.DataBoundItem;
            string nombre = $" habitacion {registroSeleccionado.numero}";
            DialogResult confirmarEliminacion = MessageBox.Show($"Desea eliminar el registro {nombre} ?","Aviso",MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (confirmarEliminacion == DialogResult.Yes)
            {
                administradorHabitaciones.eliminarHabitacion(registroSeleccionado.ID);
            }

        }

        private void btnAbrirFormCategorias_Click(object sender, EventArgs e)
        {
            new ListarCategoriasHabitaciones().Show();
        }
    }
}
