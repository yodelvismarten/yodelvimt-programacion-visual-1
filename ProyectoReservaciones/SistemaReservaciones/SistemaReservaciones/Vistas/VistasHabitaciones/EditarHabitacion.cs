﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasHabitaciones
{
    public partial class EditarHabitacion : Form
    {
        AdministradorHabitaciones administradorHabitaciones = new AdministradorHabitaciones();
        public int id;

        public EditarHabitacion()
        {
            InitializeComponent();
            new ListarHabitaciones().llenarComboBoxCategoria(this.comboBCategoria);
        }

        public void llenarFormulario(AdministradorHabitaciones objHabitacion) {
            this.id = objHabitacion.habitacion.ID;
            this.txtNumeroHabitacion.Text = objHabitacion.habitacion.numero.ToString();
            this.comboBCategoria.SelectedItem = objHabitacion.habitacion.idCategoria;
            this.txtPrecio.Text = objHabitacion.habitacion.precioXNoche.ToString();
            this.checkBEstado.Checked = objHabitacion.habitacion.estado;
        }

        public void editarObjetoHuesped()
        {
            administradorHabitaciones.asignarDatosObjeto(
                    Convert.ToInt32(txtNumeroHabitacion.Text),
                    Convert.ToInt32(comboBCategoria.SelectedValue),
                    float.Parse(txtPrecio.Text),
                    checkBEstado.Checked
                );
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            editarObjetoHuesped();
            administradorHabitaciones.editarHabitacion(this.id);

        }
    }
}
