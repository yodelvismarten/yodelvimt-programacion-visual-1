﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasHabitaciones
{
    public partial class RegistrarHabitacion : Form
    {
        AdministradorCategoriaHabitaciones administradorCategoriaHabitaciones = new AdministradorCategoriaHabitaciones();
        AdministradorHabitaciones administradorHabitaciones = new AdministradorHabitaciones();
        
        public RegistrarHabitacion()
        {
            InitializeComponent();
            new ListarHabitaciones().llenarComboBoxCategoria(this.comboBCategoria);
        }
        
        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            administradorHabitaciones.asignarDatosObjeto(
                Convert.ToInt32(txtNumeroHabitacion.Text),
                Convert.ToInt32(comboBCategoria.SelectedValue),
                float.Parse(txtPrecio.Text),
                checkBEstado.Checked
                );
            administradorHabitaciones.registrarhabitacion();
        }
    }
}
