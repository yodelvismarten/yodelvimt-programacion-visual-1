﻿namespace SistemaReservaciones.Vistas.VistasHabitaciones
{
    partial class ListarHabitaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelContenedorHuesped = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnEliminarHabitacion = new System.Windows.Forms.Button();
            this.btnAbrirFormCategorias = new System.Windows.Forms.Button();
            this.btnRegistrarHabitacion = new System.Windows.Forms.Button();
            this.btnEditarHabitacion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvHabitaciones = new System.Windows.Forms.DataGridView();
            this.panelContenedorHuesped.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHabitaciones)).BeginInit();
            this.SuspendLayout();
            // 
            // panelContenedorHuesped
            // 
            this.panelContenedorHuesped.Controls.Add(this.groupBox1);
            this.panelContenedorHuesped.Controls.Add(this.label1);
            this.panelContenedorHuesped.Controls.Add(this.textBox1);
            this.panelContenedorHuesped.Controls.Add(this.label10);
            this.panelContenedorHuesped.Controls.Add(this.dgvHabitaciones);
            this.panelContenedorHuesped.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenedorHuesped.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelContenedorHuesped.Location = new System.Drawing.Point(0, 0);
            this.panelContenedorHuesped.Name = "panelContenedorHuesped";
            this.panelContenedorHuesped.Size = new System.Drawing.Size(800, 532);
            this.panelContenedorHuesped.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnEliminarHabitacion);
            this.groupBox1.Controls.Add(this.btnAbrirFormCategorias);
            this.groupBox1.Controls.Add(this.btnRegistrarHabitacion);
            this.groupBox1.Controls.Add(this.btnEditarHabitacion);
            this.groupBox1.Location = new System.Drawing.Point(12, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(770, 84);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // btnEliminarHabitacion
            // 
            this.btnEliminarHabitacion.BackColor = System.Drawing.Color.Red;
            this.btnEliminarHabitacion.FlatAppearance.BorderSize = 0;
            this.btnEliminarHabitacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarHabitacion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarHabitacion.ForeColor = System.Drawing.Color.White;
            this.btnEliminarHabitacion.Location = new System.Drawing.Point(679, 31);
            this.btnEliminarHabitacion.Name = "btnEliminarHabitacion";
            this.btnEliminarHabitacion.Size = new System.Drawing.Size(85, 30);
            this.btnEliminarHabitacion.TabIndex = 5;
            this.btnEliminarHabitacion.Text = "Eliminar";
            this.btnEliminarHabitacion.UseVisualStyleBackColor = false;
            this.btnEliminarHabitacion.Click += new System.EventHandler(this.btnEliminarHabitacion_Click);
            // 
            // btnAbrirFormCategorias
            // 
            this.btnAbrirFormCategorias.Location = new System.Drawing.Point(6, 31);
            this.btnAbrirFormCategorias.Name = "btnAbrirFormCategorias";
            this.btnAbrirFormCategorias.Size = new System.Drawing.Size(85, 30);
            this.btnAbrirFormCategorias.TabIndex = 8;
            this.btnAbrirFormCategorias.Text = "Categorias";
            this.btnAbrirFormCategorias.UseVisualStyleBackColor = true;
            this.btnAbrirFormCategorias.Click += new System.EventHandler(this.btnAbrirFormCategorias_Click);
            // 
            // btnRegistrarHabitacion
            // 
            this.btnRegistrarHabitacion.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnRegistrarHabitacion.FlatAppearance.BorderSize = 0;
            this.btnRegistrarHabitacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistrarHabitacion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarHabitacion.ForeColor = System.Drawing.Color.White;
            this.btnRegistrarHabitacion.Location = new System.Drawing.Point(497, 31);
            this.btnRegistrarHabitacion.Name = "btnRegistrarHabitacion";
            this.btnRegistrarHabitacion.Size = new System.Drawing.Size(85, 30);
            this.btnRegistrarHabitacion.TabIndex = 3;
            this.btnRegistrarHabitacion.Text = "Registrar";
            this.btnRegistrarHabitacion.UseVisualStyleBackColor = false;
            this.btnRegistrarHabitacion.Click += new System.EventHandler(this.btnRegistrarHabitacion_Click);
            // 
            // btnEditarHabitacion
            // 
            this.btnEditarHabitacion.BackColor = System.Drawing.Color.DarkOrange;
            this.btnEditarHabitacion.FlatAppearance.BorderSize = 0;
            this.btnEditarHabitacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarHabitacion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarHabitacion.ForeColor = System.Drawing.Color.White;
            this.btnEditarHabitacion.Location = new System.Drawing.Point(588, 31);
            this.btnEditarHabitacion.Name = "btnEditarHabitacion";
            this.btnEditarHabitacion.Size = new System.Drawing.Size(85, 30);
            this.btnEditarHabitacion.TabIndex = 4;
            this.btnEditarHabitacion.Text = "Editar";
            this.btnEditarHabitacion.UseVisualStyleBackColor = false;
            this.btnEditarHabitacion.Click += new System.EventHandler(this.btnEditarHabitacion_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(800, 59);
            this.label1.TabIndex = 6;
            this.label1.Text = "Listado Habitaciones";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Bahnschrift Light", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(12, 160);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(770, 21);
            this.textBox1.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 141);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "Buscar";
            // 
            // dgvHabitaciones
            // 
            this.dgvHabitaciones.AllowUserToAddRows = false;
            this.dgvHabitaciones.AllowUserToDeleteRows = false;
            this.dgvHabitaciones.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvHabitaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHabitaciones.Location = new System.Drawing.Point(12, 187);
            this.dgvHabitaciones.Name = "dgvHabitaciones";
            this.dgvHabitaciones.ReadOnly = true;
            this.dgvHabitaciones.Size = new System.Drawing.Size(770, 327);
            this.dgvHabitaciones.TabIndex = 0;
            // 
            // ListarHabitaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 532);
            this.Controls.Add(this.panelContenedorHuesped);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ListarHabitaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ListarHabitaciones";
            this.panelContenedorHuesped.ResumeLayout(false);
            this.panelContenedorHuesped.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHabitaciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelContenedorHuesped;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEliminarHabitacion;
        private System.Windows.Forms.Button btnEditarHabitacion;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvHabitaciones;
        private System.Windows.Forms.Button btnAbrirFormCategorias;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRegistrarHabitacion;
    }
}