﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasPosiciones
{
    public partial class EditarPosicion : Form
    {
        AdministradorPosiciones administradorPosiciones = new AdministradorPosiciones();
        int id;

        public EditarPosicion()
        {
            InitializeComponent();
        }

        public void llenarFormulario(AdministradorPosiciones administradorPosiciones)
        {
            id = administradorPosiciones.posicion.ID;
            txtNombre.Text = administradorPosiciones.posicion.nombre;
            checkBEstado.Checked = administradorPosiciones.posicion.estado;
        }

        public void editarObjetoPosicion()
        {
            administradorPosiciones.asignarDatosObjeto(txtNombre.Text, checkBEstado.Checked);
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            editarObjetoPosicion();
            administradorPosiciones.editarPosicion(id);
        }
    }
}
