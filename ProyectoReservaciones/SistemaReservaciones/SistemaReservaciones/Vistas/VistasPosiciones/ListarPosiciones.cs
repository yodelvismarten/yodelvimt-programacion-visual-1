﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;
using SistemaReservaciones.LogicaNegocio.Clases.Entidades;

namespace SistemaReservaciones.Vistas.VistasPosiciones
{
    public partial class ListarPosiciones : Form
    {
        AdministradorPosiciones administradorPosiciones = new AdministradorPosiciones();

        public ListarPosiciones()
        {
            InitializeComponent();
            llenarDgv();
        }

        public void llenarDgv()
        {
            administradorPosiciones.listarPosiciones();
            dgvPosiciones.DataSource = administradorPosiciones.listaPosiciones;
            dgvPosiciones.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvPosiciones.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvPosiciones.Columns["ID"].Visible = false;
        }

        
        private void btnRegistrarPosicion_Click(object sender, EventArgs e)
        {
            new RegistrarPosicion().Show(); 
        }

        private void btnEditarPosicion_Click(object sender, EventArgs e)
        {
            this.administradorPosiciones.posicion= (Posicion)dgvPosiciones.CurrentRow.DataBoundItem;
            EditarPosicion editarPosicion = new EditarPosicion();
            editarPosicion.llenarFormulario(this.administradorPosiciones);
            editarPosicion.Show();
        }

        private void btnEliminaraPosicion_Click(object sender, EventArgs e)
        {
            this.administradorPosiciones.posicion = (Posicion)dgvPosiciones.CurrentRow.DataBoundItem;
            string nombre = administradorPosiciones.posicion.nombre.ToString();
            DialogResult confirmarEliminacion = MessageBox.Show($"Desea eliminar el registro {nombre} ?", "Aviso", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmarEliminacion == DialogResult.Yes)
            {
                administradorPosiciones.eliminarPosicion(administradorPosiciones.posicion.ID);
            }
        }
    }
}
