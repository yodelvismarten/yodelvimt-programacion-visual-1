﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasPosiciones
{
    public partial class RegistrarPosicion : Form
    {
        AdministradorPosiciones administradorPosiciones = new AdministradorPosiciones();

        public RegistrarPosicion()
        {
            InitializeComponent();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            administradorPosiciones.asignarDatosObjeto(
                txtNombre.Text,
                checkBEstado.Checked
               );
            administradorPosiciones.registrarPosicion();

        }
    }
}
