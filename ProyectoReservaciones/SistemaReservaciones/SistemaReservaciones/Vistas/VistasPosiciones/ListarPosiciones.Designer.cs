﻿namespace SistemaReservaciones.Vistas.VistasPosiciones
{
    partial class ListarPosiciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelContenedorHuesped = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnEliminaraPosicion = new System.Windows.Forms.Button();
            this.btnRegistrarPosicion = new System.Windows.Forms.Button();
            this.btnEditarPosicion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvPosiciones = new System.Windows.Forms.DataGridView();
            this.panelContenedorHuesped.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPosiciones)).BeginInit();
            this.SuspendLayout();
            // 
            // panelContenedorHuesped
            // 
            this.panelContenedorHuesped.Controls.Add(this.groupBox1);
            this.panelContenedorHuesped.Controls.Add(this.label1);
            this.panelContenedorHuesped.Controls.Add(this.textBox1);
            this.panelContenedorHuesped.Controls.Add(this.label10);
            this.panelContenedorHuesped.Controls.Add(this.dgvPosiciones);
            this.panelContenedorHuesped.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenedorHuesped.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelContenedorHuesped.Location = new System.Drawing.Point(0, 0);
            this.panelContenedorHuesped.Name = "panelContenedorHuesped";
            this.panelContenedorHuesped.Size = new System.Drawing.Size(800, 527);
            this.panelContenedorHuesped.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnEliminaraPosicion);
            this.groupBox1.Controls.Add(this.btnRegistrarPosicion);
            this.groupBox1.Controls.Add(this.btnEditarPosicion);
            this.groupBox1.Location = new System.Drawing.Point(12, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(770, 84);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // btnEliminaraPosicion
            // 
            this.btnEliminaraPosicion.BackColor = System.Drawing.Color.Red;
            this.btnEliminaraPosicion.FlatAppearance.BorderSize = 0;
            this.btnEliminaraPosicion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminaraPosicion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminaraPosicion.ForeColor = System.Drawing.Color.White;
            this.btnEliminaraPosicion.Location = new System.Drawing.Point(679, 32);
            this.btnEliminaraPosicion.Name = "btnEliminaraPosicion";
            this.btnEliminaraPosicion.Size = new System.Drawing.Size(85, 30);
            this.btnEliminaraPosicion.TabIndex = 5;
            this.btnEliminaraPosicion.Text = "Eliminar";
            this.btnEliminaraPosicion.UseVisualStyleBackColor = false;
            this.btnEliminaraPosicion.Click += new System.EventHandler(this.btnEliminaraPosicion_Click);
            // 
            // btnRegistrarPosicion
            // 
            this.btnRegistrarPosicion.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnRegistrarPosicion.FlatAppearance.BorderSize = 0;
            this.btnRegistrarPosicion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistrarPosicion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarPosicion.ForeColor = System.Drawing.Color.White;
            this.btnRegistrarPosicion.Location = new System.Drawing.Point(497, 32);
            this.btnRegistrarPosicion.Name = "btnRegistrarPosicion";
            this.btnRegistrarPosicion.Size = new System.Drawing.Size(85, 30);
            this.btnRegistrarPosicion.TabIndex = 3;
            this.btnRegistrarPosicion.Text = "Registrar";
            this.btnRegistrarPosicion.UseVisualStyleBackColor = false;
            this.btnRegistrarPosicion.Click += new System.EventHandler(this.btnRegistrarPosicion_Click);
            // 
            // btnEditarPosicion
            // 
            this.btnEditarPosicion.BackColor = System.Drawing.Color.DarkOrange;
            this.btnEditarPosicion.FlatAppearance.BorderSize = 0;
            this.btnEditarPosicion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarPosicion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarPosicion.ForeColor = System.Drawing.Color.White;
            this.btnEditarPosicion.Location = new System.Drawing.Point(588, 32);
            this.btnEditarPosicion.Name = "btnEditarPosicion";
            this.btnEditarPosicion.Size = new System.Drawing.Size(85, 30);
            this.btnEditarPosicion.TabIndex = 4;
            this.btnEditarPosicion.Text = "Editar";
            this.btnEditarPosicion.UseVisualStyleBackColor = false;
            this.btnEditarPosicion.Click += new System.EventHandler(this.btnEditarPosicion_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(800, 59);
            this.label1.TabIndex = 6;
            this.label1.Text = "Listado Posiciones";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Bahnschrift Light", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(12, 160);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(384, 21);
            this.textBox1.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 141);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "Buscar";
            // 
            // dgvPosiciones
            // 
            this.dgvPosiciones.AllowUserToAddRows = false;
            this.dgvPosiciones.AllowUserToDeleteRows = false;
            this.dgvPosiciones.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvPosiciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPosiciones.Location = new System.Drawing.Point(12, 187);
            this.dgvPosiciones.Name = "dgvPosiciones";
            this.dgvPosiciones.ReadOnly = true;
            this.dgvPosiciones.Size = new System.Drawing.Size(770, 327);
            this.dgvPosiciones.TabIndex = 0;
            // 
            // ListarPosiciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 527);
            this.Controls.Add(this.panelContenedorHuesped);
            this.Name = "ListarPosiciones";
            this.Text = "ListarPosiciones";
            this.panelContenedorHuesped.ResumeLayout(false);
            this.panelContenedorHuesped.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPosiciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelContenedorHuesped;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvPosiciones;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnEliminaraPosicion;
        private System.Windows.Forms.Button btnRegistrarPosicion;
        private System.Windows.Forms.Button btnEditarPosicion;
    }
}