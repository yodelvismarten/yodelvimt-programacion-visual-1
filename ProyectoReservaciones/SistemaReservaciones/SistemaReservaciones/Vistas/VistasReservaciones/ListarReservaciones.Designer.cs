﻿namespace SistemaReservaciones.Vistas.VistasReservaciones
{
    partial class ListarReservaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelContenedorHuesped = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnEliminarReservacion = new System.Windows.Forms.Button();
            this.btnRegistrarReservacion = new System.Windows.Forms.Button();
            this.btnEditarReservacion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvReservaciones = new System.Windows.Forms.DataGridView();
            this.panelContenedorHuesped.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReservaciones)).BeginInit();
            this.SuspendLayout();
            // 
            // panelContenedorHuesped
            // 
            this.panelContenedorHuesped.Controls.Add(this.groupBox1);
            this.panelContenedorHuesped.Controls.Add(this.label1);
            this.panelContenedorHuesped.Controls.Add(this.textBox1);
            this.panelContenedorHuesped.Controls.Add(this.label10);
            this.panelContenedorHuesped.Controls.Add(this.dgvReservaciones);
            this.panelContenedorHuesped.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenedorHuesped.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelContenedorHuesped.Location = new System.Drawing.Point(0, 0);
            this.panelContenedorHuesped.Name = "panelContenedorHuesped";
            this.panelContenedorHuesped.Size = new System.Drawing.Size(794, 526);
            this.panelContenedorHuesped.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnEliminarReservacion);
            this.groupBox1.Controls.Add(this.btnRegistrarReservacion);
            this.groupBox1.Controls.Add(this.btnEditarReservacion);
            this.groupBox1.Location = new System.Drawing.Point(12, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(770, 84);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // btnEliminarReservacion
            // 
            this.btnEliminarReservacion.BackColor = System.Drawing.Color.Red;
            this.btnEliminarReservacion.FlatAppearance.BorderSize = 0;
            this.btnEliminarReservacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarReservacion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarReservacion.ForeColor = System.Drawing.Color.White;
            this.btnEliminarReservacion.Location = new System.Drawing.Point(679, 31);
            this.btnEliminarReservacion.Name = "btnEliminarReservacion";
            this.btnEliminarReservacion.Size = new System.Drawing.Size(85, 30);
            this.btnEliminarReservacion.TabIndex = 5;
            this.btnEliminarReservacion.Text = "Eliminar";
            this.btnEliminarReservacion.UseVisualStyleBackColor = false;
            this.btnEliminarReservacion.Click += new System.EventHandler(this.btnEliminarUsuario_Click);
            // 
            // btnRegistrarReservacion
            // 
            this.btnRegistrarReservacion.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnRegistrarReservacion.FlatAppearance.BorderSize = 0;
            this.btnRegistrarReservacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistrarReservacion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarReservacion.ForeColor = System.Drawing.Color.White;
            this.btnRegistrarReservacion.Location = new System.Drawing.Point(497, 31);
            this.btnRegistrarReservacion.Name = "btnRegistrarReservacion";
            this.btnRegistrarReservacion.Size = new System.Drawing.Size(85, 30);
            this.btnRegistrarReservacion.TabIndex = 3;
            this.btnRegistrarReservacion.Text = "Registrar";
            this.btnRegistrarReservacion.UseVisualStyleBackColor = false;
            this.btnRegistrarReservacion.Click += new System.EventHandler(this.btnRegistrarUsuario_Click);
            // 
            // btnEditarReservacion
            // 
            this.btnEditarReservacion.BackColor = System.Drawing.Color.DarkOrange;
            this.btnEditarReservacion.FlatAppearance.BorderSize = 0;
            this.btnEditarReservacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarReservacion.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarReservacion.ForeColor = System.Drawing.Color.White;
            this.btnEditarReservacion.Location = new System.Drawing.Point(588, 31);
            this.btnEditarReservacion.Name = "btnEditarReservacion";
            this.btnEditarReservacion.Size = new System.Drawing.Size(85, 30);
            this.btnEditarReservacion.TabIndex = 4;
            this.btnEditarReservacion.Text = "Editar";
            this.btnEditarReservacion.UseVisualStyleBackColor = false;
            this.btnEditarReservacion.Click += new System.EventHandler(this.btnEditarUsuario_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(794, 59);
            this.label1.TabIndex = 6;
            this.label1.Text = "Listado Reservaciones";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Bahnschrift Light", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(12, 160);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(770, 21);
            this.textBox1.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 141);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "Buscar";
            // 
            // dgvReservaciones
            // 
            this.dgvReservaciones.AllowUserToAddRows = false;
            this.dgvReservaciones.AllowUserToDeleteRows = false;
            this.dgvReservaciones.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvReservaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReservaciones.Location = new System.Drawing.Point(12, 187);
            this.dgvReservaciones.Name = "dgvReservaciones";
            this.dgvReservaciones.ReadOnly = true;
            this.dgvReservaciones.Size = new System.Drawing.Size(770, 327);
            this.dgvReservaciones.TabIndex = 0;
            // 
            // ListarReservaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 526);
            this.Controls.Add(this.panelContenedorHuesped);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ListarReservaciones";
            this.Text = "ListarReservaciones";
            this.panelContenedorHuesped.ResumeLayout(false);
            this.panelContenedorHuesped.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReservaciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelContenedorHuesped;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvReservaciones;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnEliminarReservacion;
        private System.Windows.Forms.Button btnEditarReservacion;
        private System.Windows.Forms.Button btnRegistrarReservacion;
    }
}