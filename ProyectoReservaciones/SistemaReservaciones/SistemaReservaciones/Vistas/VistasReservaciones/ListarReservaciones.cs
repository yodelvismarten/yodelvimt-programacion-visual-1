﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.Entidades;
using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasReservaciones
{
    public partial class ListarReservaciones : Form
    {
        AdministradorReservaciones administradorReservaciones = new AdministradorReservaciones();

        public ListarReservaciones()
        {
            InitializeComponent();
            llenardgv();
        }

        public void llenardgv()
        {
            administradorReservaciones.listarReservacion();
            dgvReservaciones.DataSource = administradorReservaciones.listaReservaciones;
            dgvReservaciones.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvReservaciones.Columns["ID"].Visible = false;

        }

        private void btnRegistrarUsuario_Click(object sender, EventArgs e)
        {
            new RegistrarReservacion().Show();
        }

        private void btnEditarUsuario_Click(object sender, EventArgs e)
        {
            administradorReservaciones.reservacion = (Reservacion)dgvReservaciones.CurrentRow.DataBoundItem;
            EditarReservacion editarReservacion = new EditarReservacion();
            editarReservacion.llenarFormulario(administradorReservaciones);
            editarReservacion.Show();
        }

        private void btnEliminarUsuario_Click(object sender, EventArgs e)
        {
            administradorReservaciones.reservacion = (Reservacion)dgvReservaciones.CurrentRow.DataBoundItem;
            DialogResult confirmarEliminacion = MessageBox.Show("Desea eliminar esta reservacion?", "Aviso",
                                                             MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmarEliminacion == DialogResult.Yes)
            {
                administradorReservaciones.eliminarReservacion(administradorReservaciones.reservacion.ID);
            }

        }   

    }
}
