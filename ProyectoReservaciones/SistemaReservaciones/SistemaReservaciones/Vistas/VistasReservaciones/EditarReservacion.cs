﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

namespace SistemaReservaciones.Vistas.VistasReservaciones
{
    public partial class EditarReservacion : Form
    {
        AdministradorReservaciones administradorReservaciones = new AdministradorReservaciones();
        AdministradorHuespedes administradorHuespedes = new AdministradorHuespedes();
        AdministradorHabitaciones administradorHabitaciones = new AdministradorHabitaciones();
        float precioXnoche;
        int id;

        public EditarReservacion()
        {
            InitializeComponent();
            llenarComboBHuepedesYHabitaciones();
        }

        public void llenarComboBHuepedesYHabitaciones()
        {
            //LLENAR COMBOBOX HUESPED
            administradorHuespedes.listarHuespedes();
            comboBHueped.DataSource = administradorHuespedes.huespedes;
            comboBHueped.DisplayMember = "Nombre";
            comboBHueped.ValueMember = "ID";

            //LLENAR COMBOBOX HABITACION
            administradorHabitaciones.listarHabitaciones();
            comboBHabitacion.DataSource = administradorHabitaciones.listaHabitaciones;
            comboBHabitacion.DisplayMember = "numero";
            comboBHabitacion.ValueMember = "ID";
        }

        public void llenarFormulario(AdministradorReservaciones administradorReservaciones)
        {
            id = Convert.ToInt32(administradorReservaciones.reservacion.ID);
            this.comboBHueped.SelectedValue = administradorReservaciones.reservacion.idHuesped;
            this.comboBHabitacion.SelectedValue = administradorReservaciones.reservacion.idHabitacion;
            this.txtCantidad.Text = administradorReservaciones.reservacion.cantidadAcompañantes.ToString();
            this.txtCantidadDias.Text = administradorReservaciones.reservacion.cantidadDias.ToString();
            this.dtpCheckIn.Value = Convert.ToDateTime(administradorReservaciones.reservacion.fechaEntrada);
            this.dtpCheckOut.Value = Convert.ToDateTime(administradorReservaciones.reservacion.fechaSalida);
            this.lblTotal.Text = administradorReservaciones.reservacion.totalPagar.ToString();
        }

        public void calcularTotaPagar()
        {
            if (txtCantidadDias.Text != string.Empty)
            {
                var registroHabitacion = administradorHabitaciones.listaHabitaciones.Where(x => x.ID == Convert.ToInt32(comboBHabitacion.SelectedValue)).ToList();
                precioXnoche = float.Parse(registroHabitacion[0].precioXNoche.ToString());
                float total = precioXnoche * float.Parse(txtCantidadDias.Text);
                this.lblTotal.Text = total.ToString();

            }
        }

        private void dtpCheckIn_ValueChanged(object sender, EventArgs e)
        {
            dtpCheckOut.Value = dtpCheckIn.Value.AddDays(Convert.ToInt32(txtCantidadDias.Text));
        }

        private void txtCantidadDias_TextChanged(object sender, EventArgs e)
        {
            calcularTotaPagar();
        }

        public void editarObjetoReservacion()
        {
            administradorReservaciones.asignarDatosObjeto(
                Convert.ToInt32(comboBHueped.SelectedValue),
                Convert.ToInt32(comboBHabitacion.SelectedValue),
                Convert.ToInt32(txtCantidad.Text),
                dtpCheckIn.Value,
                Convert.ToInt32(txtCantidadDias.Text),
                dtpCheckOut.Value,
                Convert.ToInt32(lblTotal.Text)
                );
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            editarObjetoReservacion();
            administradorReservaciones.editarReservacion(id);
        }
    }
}
