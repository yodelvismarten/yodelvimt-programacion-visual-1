﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SistemaReservaciones.Vistas.VistasHuespedes;
using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;
using SistemaReservaciones.LogicaNegocio.Clases.Entidades;

namespace SistemaReservaciones.Vistas.VistasReservaciones
{
    public partial class RegistrarReservacion : Form
    {
        AdministradorReservaciones administradorReservaciones = new AdministradorReservaciones();
        Reservacion reservacion = new Reservacion();

        AdministradorHuespedes administradorHuespedes = new AdministradorHuespedes();
        AdministradorHabitaciones administradorHabitaciones = new AdministradorHabitaciones();
        AdministradorSesion administradorSesion = new AdministradorSesion();
        float precioXnoche;


        public RegistrarReservacion()
        {
            InitializeComponent();
            llenarComboBHuepedesYHabitaciones();
            this.dtpCheckOut.Enabled = false;
        }

        public void llenarComboBHuepedesYHabitaciones()
        {
            //LLENAR COMBOBOX HUESPED
            administradorHuespedes.listarHuespedes();
            comboBHueped.DataSource = administradorHuespedes.huespedes;
            comboBHueped.DisplayMember = "Nombre";
            comboBHueped.ValueMember = "ID";

            //LLENAR COMBOBOX HABITACION
            administradorHabitaciones.listarHabitaciones();
            comboBHabitacion.DataSource = administradorHabitaciones.listaHabitaciones;
            comboBHabitacion.DisplayMember = "numero";
            comboBHabitacion.ValueMember = "ID";
        }
        
        private void picBRegistrarHuesped_Click(object sender, EventArgs e)
        {
            new RegistrarHuesped().Show();
        }

        /// <summary>
        /// ESTE METODO OBTIENE EL REGISTRO DE LA HABITACION SELECCIONADA EN EL COMBOBOX HABITACION DEL FORMULARIO 
        /// A PARTIR DE SU ID Y LO COLOCA EN UNA LISTA DE DICCIONARIOS QUE TENDRA ESE UNICO ELEMENTO, PARA DESPUES 
        /// TOMAR SU KEY 'precioXnoche' Y CALCULAR EL PRECIO TOTAL DE LA RESERVACION, ASIGNANDO FINALMENTE EL PRECIO TOTAL
        /// AL LABEL 'totalPagar' DEL FORMULARIO
        /// </summary>

        public void calcularTotaPagar()
        {
            if(txtCantidadDias.Text != string.Empty) { 
                var registroHabitacion = administradorHabitaciones.listaHabitaciones.Where(x => x.ID == Convert.ToInt32(comboBHabitacion.SelectedValue)).ToList();
                precioXnoche = float.Parse(registroHabitacion[0].precioXNoche.ToString());
                float total = precioXnoche * float.Parse(txtCantidadDias.Text);
                this.lblTotal.Text = total.ToString();
                
            }
        }
        
        

        private void dtpCheckIn_ValueChanged(object sender, EventArgs e)
        {
            this.dtpCheckOut.Value = dtpCheckIn.Value.AddDays(Convert.ToInt32(this.txtCantidadDias.Text));
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            administradorReservaciones.asignarDatosObjeto(
                Convert.ToInt32(comboBHueped.SelectedValue),
                Convert.ToInt32(comboBHabitacion.SelectedValue),
                Convert.ToInt32(txtCantidad.Text),
                Convert.ToDateTime(dtpCheckIn.Value),
                Convert.ToInt32(txtCantidadDias.Text),
                Convert.ToDateTime(dtpCheckOut.Value),
                Convert.ToInt32(lblTotal.Text)
                );
            administradorReservaciones.registrarReservacion();
        }

        private void txtCantidadDias_TextChanged(object sender, EventArgs e)
        {
            calcularTotaPagar();
        }
    }
}
