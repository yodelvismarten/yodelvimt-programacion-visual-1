﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Controladores
using SistemaReservaciones.LogicaNegocio.Clases;
using SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades;

//Vistas
using SistemaReservaciones.Vistas.VistaInicio;
using SistemaReservaciones.Vistas.VistasUsuarios;
using SistemaReservaciones.Vistas.VistasHuespedes;
using SistemaReservaciones.Vistas.VistasHabitaciones;
using SistemaReservaciones.Vistas.VistasReservaciones;

namespace SistemaReservaciones
{
    public partial class FormularioInicio : Form
    {
        AdministradorSesion administradorSesion = new AdministradorSesion();

        public FormularioInicio()
        {
            InitializeComponent();
            new ClaseGenerica().AbrirFormHija(panelContenedorPadre, new Inicio());
        }

        /// <summary>
        /// metodo que solo mostrara el modulo para Usuarios si el usuario que inicio sesion es administrador
        /// </summary>
        public void validarPermisoUsuario(bool admin) {
            if (admin == true)
            {
                pictureBUsuarios.Visible = true;
                btnUsuarios.Visible = true;
            }
            else
            {
                pictureBUsuarios.Visible = false;
                btnUsuarios.Visible = false;
            }
        }

        private void btnHuespedes_Click(object sender, EventArgs e)
        {
            new ClaseGenerica().AbrirFormHija(panelContenedorPadre, new ListarHuespedes());
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            new ClaseGenerica().AbrirFormHija(panelContenedorPadre, new ListarUsuarios());
        }

        private void btnCerrarAplicacion_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimizarAplicacion_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnHabitaciones_Click(object sender, EventArgs e)
        {
            new ClaseGenerica().AbrirFormHija(panelContenedorPadre, new ListarHabitaciones());
            
        }

        private void btnReservaciones_Click(object sender, EventArgs e)
        {
            new ClaseGenerica().AbrirFormHija(panelContenedorPadre, new ListarReservaciones());
        }

        private void btnInicio_Click(object sender, EventArgs e)
        {
            new ClaseGenerica().AbrirFormHija(panelContenedorPadre, new Inicio());
        }
    }

    
}
