﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases;
using SistemaReservaciones.LogicaNegocio.Clases.Entidades;

namespace SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades
{
    public class AdministradorReservaciones
    {
        AdministradorSesion administradorSesion = new AdministradorSesion();
         public Reservacion reservacion = new Reservacion();

        string tabla = "reservaciones";
        

        public List<Reservacion> listaReservaciones = new List<Reservacion>();

        public void asignarDatosObjeto(int huesped, int habitacion, int acompañantes, 
            DateTime fechaEntrada, int cantidadDias, DateTime fechaSalida, float totalPagar)
        {
            reservacion.idHuesped = huesped;
            reservacion.idHabitacion = habitacion;
            reservacion.cantidadAcompañantes = acompañantes;
            reservacion.fechaEntrada = fechaEntrada;
            reservacion.cantidadDias = cantidadDias;
            reservacion.fechaSalida = fechaSalida;
            reservacion.totalPagar = totalPagar;
        }

        public void listarReservacion()
        {
            try
            {
                string query = $"SELECT * FROM {tabla}";
                MySqlDataReader reader = ClaseConexion.consultar(query);

                while (reader.Read())
                {
                    Reservacion objAuxiliar = new Reservacion();
                    objAuxiliar.ID = Convert.ToInt32(reader["id_reservacion"]);
                    objAuxiliar.idHuesped = Convert.ToInt32(reader["huesped"]);
                    objAuxiliar.idHabitacion = Convert.ToInt32(reader["habitacion"]);
                    objAuxiliar.cantidadAcompañantes = Convert.ToInt32(reader["cantidad_acompañantes"]);
                    objAuxiliar.fechaCreacion = Convert.ToDateTime(reader["fecha_creacion"]);
                    objAuxiliar.fechaEntrada = Convert.ToDateTime(reader["fecha_entrada"]);
                    objAuxiliar.cantidadDias = Convert.ToInt32(reader["cantidad_dias"]);
                    objAuxiliar.fechaSalida = Convert.ToDateTime(reader["fecha_salida"]);
                    objAuxiliar.totalPagar = float.Parse(reader["total_pagar"].ToString());
                    objAuxiliar.idUsuario = Convert.ToInt32(reader["usuario"]);

                    listaReservaciones.Add(objAuxiliar);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        public void registrarReservacion()
        {
            
            try
            {
                reservacion.fechaCreacion = DateTime.Now;
                //reservacion.idUsuario = Convert.ToInt32(administradorSesion.sesion.ID);
                reservacion.idUsuario = 1;
                string query = $@"INSERT INTO {tabla} (huesped, habitacion, cantidad_acompañantes, fecha_creacion,
                                fecha_entrada, cantidad_dias, fecha_salida, total_pagar, usuario)
                               VALUES({reservacion.idHuesped},
                                    {reservacion.idHabitacion}, {reservacion.cantidadAcompañantes},
                                    '{reservacion.fechaCreacion.ToString("yyyy-MM-dd HH:mm:ss")}',
                                    '{reservacion.fechaEntrada.ToString("yyyy-MM-dd HH:mm:ss")}',
                                    {reservacion.cantidadDias},
                                    '{reservacion.fechaSalida.ToString("yyyy-MM-dd HH:mm:ss")}',
                                    {reservacion.totalPagar},
                                    {reservacion.idUsuario})";

                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void editarReservacion(int id)
        {
            try
            {
                string query = $@"UPDATE {tabla}
                                SET
                                huesped = {reservacion.idHuesped},
                                habitacion = {reservacion.idHabitacion},
                                cantidad_acompañantes = {reservacion.cantidadAcompañantes},
                                fecha_entrada = '{reservacion.fechaEntrada.ToString("yyyy-MM-dd HH:mm:ss")}',
                                cantidad_dias = {reservacion.cantidadDias},
                                fecha_salida = '{reservacion.fechaSalida.ToString("yyyy-MM-dd HH:mm:ss")}',
                                total_pagar = {reservacion.totalPagar}
                                WHERE id_reservacion = {id}";
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void eliminarReservacion(int id)
        {
            try
            {
                string query = $@"DELETE FROM {tabla} WHERE id_reservacion = {id}";
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
