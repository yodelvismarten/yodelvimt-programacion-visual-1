﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases;
using SistemaReservaciones.LogicaNegocio.Clases.Entidades;

namespace SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades
{
    public class AdministradorCategoriaHabitaciones
    {
        //variable para indicar la tabla de la base de datos con la que trabajara este administrador
        public static string tabla = "categoria_habitaciones";

        //lista de tipo CategoriaHabitacion para almacenar categorias
        public List<CategoriaHabitacion> listaCategorias = new List<CategoriaHabitacion>();

        public CategoriaHabitacion categoriaHabitacion = new CategoriaHabitacion();

        public void asignarDatosObjeto(string nombre, CheckBox estado)
        {
            this.categoriaHabitacion.nombre = nombre;
            this.categoriaHabitacion.estado = estado.Checked;
        }

        //metodo para listar toda las categoria que de la base de datos
        public void listarCategorias()
        {
            try
            {
                //limpiar la lista de categorias en caso de que tenga contenido 
                //para evitar duplicacion de registros al consultar la base de datos
                this.listaCategorias.Clear();

                string query = $@"SELECT
                                    *
                                  FROM {tabla}
                                  WHERE estado = true";

                //creacion de objeto de tipo mysqlreader que hara la lectura o consulta a la base de datos
                MySqlDataReader reader = ClaseConexion.consultar(query);

                while (reader.Read())
                {
                    CategoriaHabitacion objAuxiliar = new CategoriaHabitacion();
                    objAuxiliar.ID = Convert.ToInt32((reader["id_categoria"]));
                    objAuxiliar.nombre = reader["nombre"].ToString();
                    objAuxiliar.fechaCreacion = Convert.ToDateTime(reader["fecha_creacion"] is DBNull ? DateTime.Now : reader["fecha_creacion"]);
                    objAuxiliar.fechaActualizado = Convert.ToDateTime(reader["fecha_actualizado"] is DBNull ? DateTime.Now : reader["fecha_actualizado"]);
                    objAuxiliar.estado = Convert.ToBoolean(reader["estado"]);

                    this.listaCategorias.Add(objAuxiliar);
                }
                //cerrar conexion a la BD
                ClaseConexion.CerrarConexion();
                
            }

            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void registrarCategoria()
        {
            try
            {
                this.categoriaHabitacion.fechaCreacion = DateTime.Now;
                string query = $@"INSERT INTO {tabla} (nombre, fecha_creacion, estado)
                                  VALUES (
                                   '{this.categoriaHabitacion.nombre}',
                                   '{this.categoriaHabitacion.fechaCreacion.ToString("yyyy-MM-dd HH:mm:ss")}',
                                   {this.categoriaHabitacion.estado})";
                ClaseConexion.ejecutarQuery(query);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void editarCategoria(int id)
        {
            try
            {
                this.categoriaHabitacion.fechaActualizado = DateTime.Now;

                string query = $@"UPDATE {tabla} 
                                SET 
                                nombre = '{categoriaHabitacion.nombre}', 
                                estado = {categoriaHabitacion.estado} 
                                WHERE id_categoria = {id}";
                MessageBox.Show(query);
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }
        }

        public void eliminarCategoria(int id)
        {
            try
            {
                string query = $@"UPDATE {tabla} 
                            SET 
                            estado = false 
                            WHERE id_categoria = {id}";
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {

                System.Windows.Forms.MessageBox.Show(e.Message);
            }
        }

    }
}
