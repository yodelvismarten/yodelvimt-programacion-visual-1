﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases;
using SistemaReservaciones.LogicaNegocio.Clases.Entidades;

namespace SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades
{
    public class AdministrarUsuarios
    {
        private string tabla = "usuarios";

        public List<Usuario> listaUsuarios = new List<Usuario>();

        public Usuario usuario = new Usuario();

        public void asignarDatosObjeto(string username, string password, string nombre, string apellido,
            string cedula, string direccion, string telefono, int idPosicion, string email, bool esAdministrador,
            bool estado)
        {
            usuario.username = username;
            usuario.password = password;
            usuario.nombre = nombre;
            usuario.apellido = apellido;
            usuario.cedula = cedula;
            usuario.direccion = direccion;
            usuario.telefono = telefono;
            usuario.idPosicion = idPosicion;
            usuario.email = email;
            usuario.esAdministrador = esAdministrador;
            usuario.estado = estado;
        }

        public void listarUsuarios()
        {
            try
            {
                this.listaUsuarios.Clear();

                string query = $@"SELECT * FROM {tabla} WHERE estado = true";
                MySqlDataReader reader = ClaseConexion.consultar(query);

                while (reader.Read())
                {
                    Usuario objAuxiliar = new Usuario();
                    objAuxiliar.ID = Convert.ToInt32(reader["id_usuario"]);
                    objAuxiliar.username = reader["username"].ToString();
                    objAuxiliar.password = reader["password"].ToString();
                    objAuxiliar.nombre = reader["nombre"].ToString();
                    objAuxiliar.apellido = reader["apellido"].ToString();
                    objAuxiliar.cedula = reader["cedula"].ToString();
                    objAuxiliar.direccion = reader["direccion"].ToString();
                    objAuxiliar.telefono = reader["telefono"].ToString();
                    objAuxiliar.idPosicion = Convert.ToInt32(reader["posicion"]);
                    objAuxiliar.email = reader["email"].ToString();
                    objAuxiliar.fechaCreacion = (reader["fecha_creacion"] is DBNull) ? DateTime.Now : Convert.ToDateTime(reader["fecha_creacion"]);
                    objAuxiliar.fechaActualizado = (reader["fecha_actualizado"] is DBNull ) ? DateTime.Now : Convert.ToDateTime(reader["fecha_actualizado"]);
                    objAuxiliar.esAdministrador = Convert.ToBoolean(reader["es_administrador"]);
                    objAuxiliar.estado = Convert.ToBoolean(reader["estado"]);

                    listaUsuarios.Add(objAuxiliar);
                }
                ClaseConexion.CerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void registrarUsuario()
        {
            try
            {
                usuario.fechaCreacion = DateTime.Now;
                string query = $@" INSERT INTO {tabla}
                (username, password, nombre, apellido, cedula, direccion, telefono, 
                posicion, email, fecha_creacion, es_administrador, estado)
                VALUES('{usuario.username}', '{usuario.password}','{usuario.nombre}', '{usuario.apellido}',
                '{usuario.cedula}','{usuario.direccion}', '{usuario.telefono}',{usuario.idPosicion}, 
                '{usuario.email}','{usuario.fechaCreacion.ToString("yyyy-MM-dd HH:mm:ss")}', {usuario.esAdministrador}, {usuario.estado})";

                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void editarUsuario(int id)
        {
            try
            {
                usuario.fechaActualizado = DateTime.Now;
                string query = $@"UPDATE {tabla}
                                SET
                                username = '{usuario.username}',
                                password = '{usuario.password}',
                                nombre = '{usuario.nombre}',
                                apellido = '{usuario.apellido}',
                                cedula = '{usuario.cedula}',
                                direccion = '{usuario.direccion}',
                                telefono = '{usuario.telefono}',
                                posicion = {usuario.idPosicion},
                                email = '{usuario.email}',
                                fecha_actualizado = '{usuario.fechaActualizado}',
                                es_administrador = {usuario.esAdministrador},
                                estado = {usuario.estado}
                                WHERE id_usuario = {id}";
                ClaseConexion.ejecutarQuery(query);
                }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void eliminarUsuario(int id)
        {
            try
            {
                string query = $"UPDATE {tabla} SET estado = false WHERE id_usuario = {id}";
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
