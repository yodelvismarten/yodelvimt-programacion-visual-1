﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;


using SistemaReservaciones.LogicaNegocio.Clases.Entidades;
using SistemaReservaciones.LogicaNegocio.Clases;

namespace SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades
{
    public class AdministradorSesion
    {
        public static string tabla = "usuarios";

        public Sesion sesion = new Sesion();

        //metodo login en el que si se encuentra en la tabla "usuarios" el username y la password pasada por el usuario,
        //se creara una sesion con los datos del usuario que inicio sesion
        public bool login(string username, string password)
        {
            bool usuarioValido = true;

            try
            {
                string query = $@"SELECT id_usuario, nombre, apellido, username, es_administrador FROM {tabla} 
                           WHERE username = '{username}' AND password = '{password}' AND estado = true";

                MySqlDataReader reader = ClaseConexion.consultar(query);

                while (reader.Read())
                {
                    sesion.ID = Convert.ToInt32(reader["id_usuario"]);
                    sesion.username = reader["username"].ToString();
                    sesion.nombre = reader["nombre"].ToString();
                    sesion.apellido = reader["apellido"].ToString();
                    sesion.esAdministrador = Convert.ToBoolean(reader["es_administrador"]);
                }
                ClaseConexion.CerrarConexion();
                return usuarioValido;
            }
            catch(Exception)
            {
                usuarioValido = false;
                return usuarioValido;
            }
            
        }

        public void logout()
        {
            sesion = new Sesion();
        }

    }
}
