﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

//using SistemaReservaciones.LogicaNegocio.Clases;
using SistemaReservaciones.LogicaNegocio.Clases.Entidades;


namespace SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades
{
    public class AdministradorHuespedes
    {
        //variable para indicar la tabla de la base de datos con la que trabajara este administrador
        public static string tabla = "huespedes";

        //lista de tipo huesped para almacenar y crear huespedes
        public List<Huesped> huespedes = new List<Huesped>();

        //objeto de tipo huesped

        public Huesped huesped = new Huesped();
        


        #region METODOS PARA MANEJAR LA CLASE "HUESPED"

        //metodo para asignar datos al objeto de tipo Huesped
        public void asignarDatosObjeto(string nombre, string apellido, ComboBox tipoDocumento,
            string numeroDocumento, string telefono, CheckBox estado)
        {
            this.huesped.nombre = nombre;
            this.huesped.apellido = apellido;
            this.huesped.tipoDocumento = tipoDocumento.SelectedItem.ToString();
            this.huesped.numeroDocumento = numeroDocumento;
            this.huesped.telefono = telefono;
            this.huesped.estado = estado.Checked;
        }

           
        //metodo para listar todos los huespedes activos de la base de datos
        public void listarHuespedes() {
            
            //vaciar la lista en caso de que tenga elementos dentro
            this.huespedes.Clear();

            try
            {

                //query de consulta
                string query = $@"
                SELECT 
                    *
                FROM 
                    {tabla}
                WHERE estado = true";

                
                //objeto que servira como lector de registros
                MySqlDataReader reader = ClaseConexion.consultar(query);
                
                //bucle para lectura de registros
                while (reader.Read())
                {
                    Huesped objAuxiliar = new Huesped();
                    objAuxiliar.ID = Convert.ToInt32(reader["id_huesped"]);
                    objAuxiliar.nombre = reader["nombre"].ToString();
                    objAuxiliar.apellido = reader["apellido"].ToString();
                    objAuxiliar.tipoDocumento = reader["tipo_documento"].ToString();
                    objAuxiliar.numeroDocumento = reader["numero_documento"].ToString();
                    objAuxiliar.telefono = reader["telefono"].ToString();
                    objAuxiliar.fechaCreacion = Convert.ToDateTime(reader["fecha_creacion"]);
                    objAuxiliar.fechaActualizado = Convert.ToDateTime(reader["fecha_actualizado"] is DBNull ? DateTime.Now : reader["fecha_actualizado"]);
                    objAuxiliar.estado = Convert.ToBoolean(reader["estado"]);

                    //agregando los objetos de tipo huesped a la lista de huespedes
                    this.huespedes.Add(objAuxiliar);
                }
                
                ClaseConexion.CerrarConexion();

            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }

        }

        //metodo para registrar un nuevo huesped
        public void registrarHuesped()
        {
            try
            {
                this.huesped.fechaCreacion = DateTime.Now;

                string query = $@"INSERT INTO {tabla} 
                (nombre, apellido, tipo_documento, numero_documento, telefono, fecha_creacion, estado) 
                VALUES(
                '{huesped.nombre}', 
                '{huesped.apellido}', 
                '{huesped.tipoDocumento}', 
                '{huesped.numeroDocumento}',
                '{huesped.telefono}',
                '{huesped.fechaCreacion.ToString("yyyy-MM-dd HH:mm:ss")}',
                {huesped.estado})";
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }

        }

        //metodo para editar un huesped existente
        public void editarHuesped(int id)
        {
            
            try
            {
                this.huesped.fechaActualizado = DateTime.Now;

                string query = $@"UPDATE {tabla} 
                SET nombre = '{huesped.nombre}',
                apellido = '{huesped.apellido}',
                tipo_documento = '{huesped.tipoDocumento}',
                numero_documento = '{huesped.numeroDocumento}',
                telefono = '{huesped.telefono}',
                fecha_actualizado = '{huesped.fechaActualizado.ToString("yyyy-MM-dd HH:mm:ss")}',
                estado = {huesped.estado}
                WHERE id_huesped = {id}";
            
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }
        }

        //metodo para realizar la eliminacion logica de un huesped
        public void eliminarHuesped(int id) {
            
                try
                {
                    string query = $@"UPDATE {tabla} SET estado = false WHERE id_huesped = {id}";
                    ClaseConexion.ejecutarQuery(query);
                }
                catch (Exception e)
                {
                    System.Windows.Forms.MessageBox.Show(e.Message);
                }
        }

        #endregion
    }
}
