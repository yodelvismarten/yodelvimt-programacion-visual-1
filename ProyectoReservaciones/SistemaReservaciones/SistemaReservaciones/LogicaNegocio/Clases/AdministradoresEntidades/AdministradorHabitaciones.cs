﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

using SistemaReservaciones.LogicaNegocio.Clases;
using SistemaReservaciones.LogicaNegocio.Clases.Entidades;

namespace SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades
{
    public class AdministradorHabitaciones
    {
        private string tabla = "habitaciones";

        public List<Habitacion> listaHabitaciones = new List<Habitacion>();

        public Habitacion habitacion = new Habitacion();

        public void asignarDatosObjeto(int numero, int idcategoria, float precioXNoche, bool estado)
        {
            this.habitacion.numero = numero;
            this.habitacion.idCategoria = idcategoria;
            this.habitacion.precioXNoche = precioXNoche;
            this.habitacion.estado = estado;
        }

        public void listarHabitaciones()
        {
            try
            {
                this.listaHabitaciones.Clear();
                string query = $@"SELECT * FROM {tabla} WHERE estado = true";
                
                //objeto que servira como lector de registros
                MySqlDataReader reader = ClaseConexion.consultar(query);
                while (reader.Read())
                {                    
                    Habitacion objAuxiliar = new Habitacion();

                    objAuxiliar.ID = Convert.ToInt32(reader["id_habitacion"]);
                    objAuxiliar.numero = Convert.ToInt32(reader["numero"]);
                    objAuxiliar.idCategoria = Convert.ToInt32(reader["categoria"]);
                    objAuxiliar.precioXNoche = float.Parse(reader["precioXnoche"].ToString());
                    objAuxiliar.fechaCreacion = Convert.ToDateTime(reader["fecha_creacion"]);
                    
                    objAuxiliar.fechaActualizado = Convert.ToDateTime(reader["fecha_actualizado"] is DBNull ? DateTime.Now : reader["fecha_actualizado"]);

                    objAuxiliar.estado = Convert.ToBoolean(reader["estado"]);

                    this.listaHabitaciones.Add(objAuxiliar);
                }
                ClaseConexion.CerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void registrarhabitacion()
        {
            try
            {
                habitacion.fechaCreacion = DateTime.Now;
                string query = $@"INSERT INTO {tabla} (numero, categoria, precioXnoche, fecha_creacion, estado) 
                                VALUES('{habitacion.numero}',
                                        {habitacion.idCategoria}, 
                                        {habitacion.precioXNoche}, 
                                        '{habitacion.fechaCreacion.ToString("yyyy-MM-dd HH:mm:ss")}', 
                                        {habitacion.estado})";
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void editarHabitacion(int id)
        {
            try
            {
                this.habitacion.fechaActualizado = DateTime.Now;

                string query = $@"UPDATE {tabla} 
                                SET 
                                numero = {habitacion.numero},
                                categoria = {habitacion.idCategoria},
                                precioxnoche = {habitacion.precioXNoche},
                                fecha_actualizado = '{habitacion.fechaActualizado.ToString("yyyy-MM-dd HH:mm:ss")}',
                                estado = {habitacion.estado}
                                WHERE id_habitacion = {id}";
                MessageBox.Show(query);
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }   
        }

        public void eliminarHabitacion(int id)
        {
            try
            {
                String query = $@"UPDATE {tabla} SET estado = false WHERE id_habitacion = {id}";
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



    }
}
