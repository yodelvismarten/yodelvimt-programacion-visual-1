﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

using SistemaReservaciones.LogicaNegocio.Clases.Entidades;

namespace SistemaReservaciones.LogicaNegocio.Clases.AdministradoresEntidades
{
    public class AdministradorPosiciones
    {
        static string tabla= "posiciones";

        public List<Posicion> listaPosiciones = new List<Posicion>();

        public Posicion posicion = new Posicion();

        public void asignarDatosObjeto(string nombre, bool estado)
        {
            posicion.nombre = nombre;
            posicion.estado = estado;
        }

        public void listarPosiciones()
        {
            try
            {
                listaPosiciones.Clear();

                string query = $"SELECT * FROM {tabla} WHERE estado = true";
                MySqlDataReader reader = ClaseConexion.consultar(query);

                while (reader.Read())
                {
                    Posicion objAuxiliar = new Posicion();
                    objAuxiliar.ID = Convert.ToInt32(reader["id_posicion"]);
                    objAuxiliar.nombre = reader["nombre"].ToString();
                    objAuxiliar.estado = Convert.ToBoolean(reader["estado"]);

                    listaPosiciones.Add(objAuxiliar);
                }
                ClaseConexion.CerrarConexion();
                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void registrarPosicion() {

            try
            {
                string query = $@"INSERT INTO {tabla}(nombre, estado) VALUES ('{posicion.nombre}', {posicion.estado})";
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        public void editarPosicion(int id)
        {
            try
            {
                string query = $@"UPDATE {tabla} SET nombre = '{posicion.nombre}', estado = {posicion.estado} WHERE id_posicion = {id}";
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }     
        }

        public void eliminarPosicion(int id)
        {
            try
            {
                string query = $@"UPDATE {tabla} SET estado = false WHERE id_posicion = {id}";
                ClaseConexion.ejecutarQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

    }
}
