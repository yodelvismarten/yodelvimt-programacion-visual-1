﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaReservaciones.LogicaNegocio.Clases
{
    public class ClaseGenerica
    {
        
        //funcion para mostrar formularios hijos dentro de un formulario padre
        public void AbrirFormHija(Panel panelPadre, object formHija)
        {
            if (panelPadre.Controls.Count > 0)
            {
                panelPadre.Controls.RemoveAt(0);
            }

            Form fh = formHija as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            panelPadre.Controls.Add(fh);
            panelPadre.Tag = fh;
            fh.BringToFront();
            fh.Show();

        }

    }
}
