﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaReservaciones.LogicaNegocio.Clases.Entidades
{
    public class Huesped
    {
        //ID del huesped
        public int ID { set; get; }
        
        //Nombre del huesped
        public string nombre { set; get; }

        //Apellido del huesped
        public string apellido { set; get; }

        //tipo de documento con el que el huesped se registrara
        public string tipoDocumento { set; get; }

        //numero de documento del huesped
        public string numeroDocumento { set; get; }

        //numero de telefono del huesped
        public string telefono { set; get; }

        //fecha de creacion del huesped
        public DateTime fechaCreacion { set; get; }

        //fecha de actualizacion del huesped
        public DateTime fechaActualizado { set; get; }

        //estado del huesped (activado / desactivado)
        public bool estado { set; get; }
    }
}
