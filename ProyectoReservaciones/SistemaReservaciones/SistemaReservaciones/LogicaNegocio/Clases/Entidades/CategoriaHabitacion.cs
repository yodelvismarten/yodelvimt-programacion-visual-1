﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaReservaciones.LogicaNegocio.Clases.Entidades
{
    public class CategoriaHabitacion
    {
        //ID de la categoria
        public int ID { set; get; }

        //nombre de la categoria
        public string nombre { set; get; }

        //fecha de creacion de la categoria
        public DateTime fechaCreacion { set; get; }

        //fecha de actualizacion de la categoria
        public DateTime fechaActualizado { set; get; }

        //estado de la categoria (activado / desactivado)
        public bool estado { set; get; }
    }
}
