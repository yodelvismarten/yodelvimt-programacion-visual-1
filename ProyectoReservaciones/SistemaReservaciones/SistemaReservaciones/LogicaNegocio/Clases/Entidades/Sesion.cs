﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaReservaciones.LogicaNegocio.Clases.Entidades
{
    public class Sesion
    {
        public int ID { set; get; }

        public string username { set; get; } 

        public string password { set; get; }

        public string nombre { set; get; }

        public string apellido { set; get; }

        public bool esAdministrador { set; get; }
    }
}
