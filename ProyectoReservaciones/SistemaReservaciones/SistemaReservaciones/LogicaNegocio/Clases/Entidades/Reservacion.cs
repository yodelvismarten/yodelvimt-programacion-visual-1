﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaReservaciones.LogicaNegocio.Clases.Entidades
{
    public class Reservacion
    {
        //ID de la reservacion
        public int ID { set; get; }

        //ID del huesped que hizo la reservacion
        public int idHuesped { set; get; }

        //ID de la habitacion que usara el huesped
        public int idHabitacion { set; get; }

        //cantidad de acompañantes del huesped
        public int cantidadAcompañantes { set; get; }

        //fecha en la que hizo la reservacion
        public DateTime fechaCreacion { set; get; }

        //fecha de check-in
        public DateTime fechaEntrada { set; get; }

        //dias que dura la reservacion de la habitacion
        public int cantidadDias { set; get; }

        //fecha de check-out de la reservacion
        public DateTime fechaSalida { set; get; }
        
        //total a pagar por la reservacion
        public float totalPagar { set; get; }

        //ID del usuario que realizo la reservacion
        public int idUsuario { set; get; }
    }
}
