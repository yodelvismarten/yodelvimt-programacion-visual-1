﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaReservaciones.LogicaNegocio.Clases.Entidades
{
   public class Habitacion
   {
        //ID de la habitacion
        public int ID { set; get; }

        //numero de la habitacion
        public int numero { set; get; }

        //ID de la categoria a la que pertenece la habitacion
        public int idCategoria { set; get; }

        //Precio por noche de la habitacion
        public float precioXNoche { set; get; }

        //fecha de creacion de la habitacion
        public DateTime fechaCreacion { set; get; }

        //fecha de actualizacion de la habitacion
        public DateTime fechaActualizado { set; get; }

        //estado de la habitacion (activado / desactivado)
        public bool estado { set; get; }
    }
}

