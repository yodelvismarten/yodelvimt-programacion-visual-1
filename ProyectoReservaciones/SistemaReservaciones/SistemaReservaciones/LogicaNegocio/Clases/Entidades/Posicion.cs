﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaReservaciones.LogicaNegocio.Clases.Entidades
{
    public class Posicion
    {
        //ID de la posicion
        public int ID { set; get; }

        //nombre de la posicion
        public string nombre { set; get; }

        //estado de la posicion (activado / desactivado)
        public bool estado { set; get; }
    }
}
