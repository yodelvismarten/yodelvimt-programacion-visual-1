﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaReservaciones.LogicaNegocio.Clases.Entidades
{
    public class Usuario
    {   // ID del usuario
        public int ID { set; get; }

        //username del usuario
        public string username { set; get; }

        //clave del usuario
        public string password { set; get; }

        //nombre del usuario
        public string nombre { set; get; }

        //apellido del usuario
        public string apellido { set; get; }

        //cedula del usuario
        public string cedula { set; get; }

        //direccion del usuario
        public string direccion { set; get; }

        //telefono del usuario
        public string telefono { set; get; }

        //ID de la posicion(puesto de trabajo) del usuario
        public int idPosicion { set; get; }

        //email del usuario
        public string email { set; get; }

        //fecha en la que fue creado el usuario
        public DateTime fechaCreacion { set; get; }

        //fecha en la que fue actualizado el usuario
        public DateTime fechaActualizado { set; get; }

        //Booleano que indica si el usuario es administrador o no
        public bool esAdministrador { set; get; }

        //Booleano que indica si el usuario esta activo o no
        public bool estado { set; get; }

    }
}
