﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
namespace SistemaReservaciones.LogicaNegocio.Clases
{
    static class ClaseConexion
    {
        //Propiedades para establecer la conexion a una base de datos MySql o MariaDB

        static string datasourse = "127.0.0.1";
        static string port = "3306";
        static string username = "root";
        static string password = "";
        static string database = "sistema_reservaciones";

        private static MySql.Data.MySqlClient.MySqlConnection cn = null;

        /// <summary>
        /// METODO PARA ESTABLECER Y ABRIR UNA CONEXION A LA BASE DE DATOS
        /// </summary>
        /// <returns>  RETORNA UNA CONEXION ABIERTA </returns>
        public static void conectarBD() {
            cn = new MySqlConnection($"datasource = {datasourse}; port = {port}; username = {username}; password = {password}; database = {database}; Convert Zero DateTime = True; Allow Zero DateTime = True");
            cn.Open();
        }

        //METODO PARA CERRAR LA CONEXION A LA BASE DE DATOS
        public static void CerrarConexion(){
            cn.Close();
        }

        //METODO PARA EJECUTAR LA INSTRUCCION DE INSERTAR, EDITAR O ELIMINAR A LA BASE DE DATOS
        public static void ejecutarQuery( string query ) {
            conectarBD();
            MySqlCommand ejecutador = cn.CreateCommand();
            ejecutador.CommandText = query;
            ejecutador.ExecuteNonQuery();
            CerrarConexion();
        }
        /// <summary>
        /// METODO PARA EXTRAER INFORMACION DE LA BASE DE DATOS A PARTIR DE UN STRING QUE HARA LA CONSULTA
        /// </summary>
        /// <param name="query"></param>
        /// <returns>RETORNA UN DATAREADER CON LA INFORMACION DESEADA</returns>
        public static MySqlDataReader consultar(string query) {
            conectarBD();
            MySqlCommand ejecutador = cn.CreateCommand();
            ejecutador.CommandText = query;
            return ejecutador.ExecuteReader();
        }
    }
}
 