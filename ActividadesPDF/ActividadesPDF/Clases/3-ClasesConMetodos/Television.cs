﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActividadesPDF.Clases._3_ClasesConMetodos
{
    class Television
    {
        public void metodoNoRetornaDato1()
        {
            MessageBox.Show("Este primer metodo no retorna ningun valor");
        }

        public void metodoNoRetornaDato2()
        {
            MessageBox.Show("Este segundo metodo no retorna ningun valor");
        }

        public int metodoRetornaEntero()
        {
            return 10;
        }

        public string metodoRetornaTexto()
        {
            return "El metodo retorno un texto";
        }
    }
}
