﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadesPDF.Clases._2_ClasesConAtributos
{
    class Avion
    {
        public string marca { set; get; }
        public string color { set; get; }
        public int capacidadUsuarios { set; get; }
        public double peso { set; get; }
        public string motor { set; get; }
    }
}
