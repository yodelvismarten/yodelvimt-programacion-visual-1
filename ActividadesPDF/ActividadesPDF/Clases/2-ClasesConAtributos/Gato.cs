﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadesPDF.Clases._2_ClasesConAtributos
{
    class Gato
    {
        public string nombre { set; get; }
        public string raza { set; get; }
        public string color { set; get; }
        public int edad { set; get; }
        public bool vacunado { set; get; }
        public string sexo { set; get; }
    }
}
