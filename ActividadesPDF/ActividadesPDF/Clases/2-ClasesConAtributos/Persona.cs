﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadesPDF.Clases._2_ClasesConAtributos
{
    public class Persona
    {
        public string nombre { set; get; }
        public string apellido { set; get; }
        public string Sexo { set; get; }
        public string cedula { set; get; }
        public int edad { set; get; }
    }
}
