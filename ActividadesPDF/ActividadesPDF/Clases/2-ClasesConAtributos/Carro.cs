﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadesPDF.Clases._2_ClasesConAtributos
{
    class Carro
    {
        public string marca { set; get; }
        public string modelo { set; get; }
        public int cantidadPuertas { set; get; }
        public string color { set; get; }
        public string matricula { set; get; }
    }
}
